<?php
    //_DISABLED = 0
    //_NONE = 1
    //_ACTIVE = 2
    //if (session_status() != 2) {
    //    session_start();
    //}
    $originID = 1;
    foreach($arr as $song) {
        if(empty($song['cover_location'])) {
            $cover_location = 'http://osi.iptime.org/cloud/'.$_SESSION['mailAddr'].'/WebPlayer/covers/sample/sample.jpg';
        //    $cover_location = 'http://osi.iptime.org/cloud/osi/WebPlayer/covers/sample/sample.jpg';
        } else {
            $cover_location = 'http://osi.iptime.org'.$song['cover_location'];
        }
        if(empty($song['lyrics'])) {
            $lyrics = '0';
        } else {
            $lyrics = '1';
        }
        if(empty($song['title'])) {
            $title = $song['file_name'];
        } else {
            $title = $song['title'];
        }
?>
    <section origin_id="<?=$originID?>" class="songSec" src="http://osi.iptime.org<?=$song['file_location']?>" type="<?=$song['file_mime']?>">
        <section class="songImgSec_list">
            <img class="songCover_list" src="<?=$cover_location?>" crossorigin="Anonymous"/>
        </section>
        <section class="songTextSec_list">
            <p class="songTitle_list"><?=$title?></p>
            <p class="songArtist_list"><?=$song['artist']?></p>
            <p class="songAlbum_list"><?=$song['album']?></p>
            <p class="songGenre_list display_none"><?=$song['genre']?></p>
            <p class="songTime_list">
                <span class="songTimeChild_list"><?=$song['playtime']?></span>
                <span></span>
            </p>
        </section>
    </section>
<?php
        $originID++;
    }
?>