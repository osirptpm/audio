<?php
    if(!empty($data)){
        print_r($data);
?>
<script src="/public/js/oMusic_player.js" type="text/javascript"></script>
<article id="player" class="">
    <aside id="player_selectList" class=""> <!-- mobileMode -->
        <nav>
            <section class="selectListChildSec">
                <div class="selectList_btn">OSI</div>
            </section>
            <section class="selectListChildSec">
                <div class="selectList_btn">전체</div>
                <div class="selectList_btn">현재</div>
                <div class="selectList_btn">즐찾</div>
            </section>
            <section class="selectListChildSec">
                <div class="selectList_btn">
                    <div class="uPDownDiv dynamicIcon">
                        <div class="uPDownDiv_bar"></div>
                        <div class="uPDownDiv_bar"></div>
                        <div class="uPDownDiv_bar"></div>
                        <div class="uPDownDiv_bar"></div>
                    </div>
                </div>
            </section>
            <section class="selectListChildSec">
                <div class="selectList_btn">
                    <div class="settingDiv dynamicIcon">
                        <div class="settingDiv_bar">
                            <div class="settingDiv_ball"></div>
                        </div>
                        <div class="settingDiv_bar">
                            <div class="settingDiv_ball"></div>
                        </div>
                    </div>
                </div>
            </section>
        </nav>
    </aside>
    <section id="player_board">
        <div class="songList">
            <div class="list_type_A">
<?php
        foreach($data["allList"] as $song) {
            if(empty($song->{'cover_location'})) {
                $cover_location = 'http://osi.iptime.org/cloud/'.$_GET['mailAddr'].'/WebPlayer/covers/sample/sample.jpg';
            //    $cover_location = 'http://osi.iptime.org/cloud/osi/WebPlayer/covers/sample/sample.jpg';
            } else {
                $cover_location = 'http://osi.iptime.org'.$song->{'cover_location'};
            }
            if(empty($song->{'lyrics'})) {
                $lyrics = '0';
            } else {
                $lyrics = '1';
            }
            if(empty($song->{'title'})) {
                $title = $song->{'file_name'};
            } else {
                $title = $song->{'title'};
            }

?>
                <section class="songSec" src="http://osi.iptime.org<?=$song -> {'file_location'}?>" type="<?=$song -> {'file_mime'}?>">
                    <section class="songImgSec_list">
                        <img class="songCover_list" src="<?=$cover_location?>"/>
                    </section>
                    <section class="songTextSec_list">
                        <p class="songTitle_list"><?=$title?></p>
                        <p class="songArtist_list"><?=$song -> {'artist'}?></p>
                        <p class="songAlbum_list"><?=$song -> {'album'}?></p>
                        <p class="songTime_list">
                            <span class="songTimeChild_list"><?=$song -> {'playtime'}?></span>
                            <span></span>
                        </p>
                    </section>
                </section>
<?php
        }
?>
            </div>
        </div>
    </section>
    <section id="player_playing" class=""> <!-- view_playingSec -->
        <audio id="myAudio"></audio>
        <div id="songDiv_current">
            <section id="songCoverSec_current">
                <div>
                    <img id="songCover_current" src="http://osi.iptime.org/cloud/osi/WebPlayer/covers/01%20Say%20Something.jpeg"/>
                </div>
            </section>
            <div id="songInfoDiv_current" class=""> <!-- view_associateSec -->
                <section id="songCtrlSec_current">
                    <div id="currentSongProgress_current"></div>
                    <section id="songCtrlBtnSec_current">
                        <div class="repeat songCtrlBtn_current icon-48">
                            <div></div>
                            <div></div>
                        </div>
                        <div class="prev songCtrlBtn_current icon-48">
                            <div>
                                <div></div>
                                <div></div>
                            </div>
                            <div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                        <div class="play songCtrlBtn_current icon-64">
                            <div></div>
                            <div></div>
                        </div>
                        <div class="next songCtrlBtn_current icon-48">
                            <div>
                                <div></div>
                                <div></div>
                            </div>
                            <div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                        <div class="shuffle songCtrlBtn_current icon-48">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </section>
                    <div id="currentOherProgress_current"></div>
                </section>
                <section id="songAssocCtrlBtnSec_current">
                    <div id="songAssocCtrlBtnDiv_current">
                        <div class="songAssocCtrlBtn_current"><span>가수</span></div>
                        <div class="songAssocCtrlBtn_current"><span>앨범</span></div>
                        <div class="songAssocCtrlBtn_current"><span>장르</span></div>
                    </div>
                </section>
                <section id="songAssocSec_current">
                    <div class="list_type_A">
<?php
        foreach($data as $song) {
            if(empty($song->{'cover_location'})) {
                $cover_location = 'http://osi.iptime.org/cloud/'.$_GET['mailAddr'].'/WebPlayer/covers/sample/sample.jpg';
            //    $cover_location = 'http://osi.iptime.org/cloud/osi/WebPlayer/covers/sample/sample.jpg';
            } else {
                $cover_location = 'http://osi.iptime.org'.$song->{'cover_location'};
            }
            if(empty($song->{'lyrics'})) {
                $lyrics = '0';
            } else {
                $lyrics = '1';
            }
            if(empty($song->{'title'})) {
                $title = $song->{'file_name'};
            } else {
                $title = $song->{'title'};
            }

?>
                        <section class="songSec" src="<?=$song -> {'file_location'}?>" type="<?=$song -> {'file_mime'}?>">
                            <section class="songImgSec_list">
                                <img class="songCover_list" src="<?=$cover_location?>"/>
                            </section>
                            <section class="songTextSec_list">
                                <p class="songTitle_list"><?=$title?></p>
                                <p class="songArtist_list"><?=$song -> {'artist'}?></p>
                                <p class="songAlbum_list"><?=$song -> {'album'}?></p>
                                <p class="songTime_list">
                                    <span class="songTimeChild_list"><?=$song -> {'playtime'}?></span>
                                    <span></span>
                                </p>
                            </section>
                        </section>
<?php
        }
?>
                    </div>
                </section>
            </div>
        </div>
    </section>
</article>

<?php
    }
 ?>