<!DOCTYPE html>

<HTML>
<HEAD>
    <meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, height=device-height, user-scalable=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="theme-color" content="rgba(52, 152, 219, 1)">
	<meta name="application-name" content="oMusic">
    <title>oMusic</title>
    <link rel="shortcut icon" sizes="128x128" href="/public/images/1468294622_office-80.png">
	<link href="/public/css/backgroundDiv_scroll.css" type="text/css" rel="stylesheet" >
	<link href="/public/css/Icons.css" type="text/css" rel="stylesheet" >
	<link href="/public/css/allPages.css" type="text/css" rel="stylesheet" >
	<link href="/public/css/page_one.css" type="text/css" rel="stylesheet" >
	<link href="/public/css/page_two.css" type="text/css" rel="stylesheet" >
	<link href="/public/css/oMusic_player.css" type="text/css" rel="stylesheet" >
	<link href="/public/css/oMusic_player_listTypes.css" type="text/css" rel="stylesheet" >
    <script src="/public/js/jquery-3.0.0.min.js" type="text/javascript"></script>
    <script src="/public/js/jquery-ui-1.11.4/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/public/js/color-thief.min.js" type="text/javascript"></script>
    <script src="/public/js/oFunctions-0.1.js" type="text/javascript"></script>
    <style type="text/css">
    </style>
    <!--[if lt IE 9]>
      <script>
        document.createElement("header" );
        document.createElement("footer" );
        document.createElement("section");
        document.createElement("aside"  );
        document.createElement("nav"    );
        document.createElement("article");
        document.createElement("hgroup" );
        document.createElement("time"   );
      </script>
      <noscript>
         <strong>주의 !</strong>
         귀하가 사용하고 계신 브라우저는 HTML5를 지원하고 있지 않아서, 할 수 없이 JScript를 써서 몇몇 요소가 제대로 보이도록 구현했습니다.
         그런데 안타깝게도 귀하의 브라우저에선 스크리트 기능이 꺼져있습니다. 이 페이지가 제대로 표시되려면 스크립트 기능을 켜주셔야만 합니다.
       </noscript>
    <![endif]-->
    <script type="text/javascript">
    </script>
</HEAD>