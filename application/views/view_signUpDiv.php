<div id="signUp_inBox">
    <form id="signUp_form" method="post" action="/application/controllers/Controller.php">
        <p>계정 만들기</p>
        <input id="signUp_Email" class="signUp_input" name="Email" type="email" placeholder="메일 주소" required/>
        <input id="signUp_Name" class="signUp_input" name="Name" type="text" placeholder="아이디" required/>
        <input id="signUp_Passwd" class="signUp_input" name="Passwd" type="password" placeholder="암호" required/>
        <input id="signUp_Passwd_check" class="signUp_input" name="Passwd_check" type="password" placeholder="암호 확인" required/>
        <input id="signUp_submit" class="signUp_input" type="submit" value="등록"/>
    </form>
</div>