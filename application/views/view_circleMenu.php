<div class="circleMenu_playingDiv">
    <div class="circleMenu_controlDiv">
        <img class="circleMenu_albumCover" crossOrigin="anonymous"/>
        <div class="childCircle1 circleMenu_childCircle"><p>보기</p></div>
        <div class="childCircle2 circleMenu_childCircle"><p>섞기</p></div>
        <div class="childCircle3 circleMenu_childCircle"><p>반복</p></div>
        <div class="childCircle4 circleMenu_childCircle"><p>다음</p></div>
        <div class="childCircle5 circleMenu_childCircle"><p>이전</p></div>
        <div class="childCircle6 circleMenu_childCircle"><p>재생</p></div>
        <div class="childCircle7 circleMenu_childCircle"><p>감소</p></div>
        <div class="childCircle8 circleMenu_childCircle"><p>증가</p></div>
        <div class="childCircle9 circleMenu_childCircle"><p>가사</p></div>
        <div id="circlePlayer_infoBox">
            <p id="circlePlayer_title" class="circlePlayer_textInfo">Say Something</p>
            <p id="circlePlayer_artist" class="circlePlayer_textInfo">윤하</p>
        </div>
    </div>
</div>