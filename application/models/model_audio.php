<?php
    class model_audio extends model_SQLQuery {
        function checkID($checkID) {
            $query = $this -> selectAll('memver.memvers', 'uName');
            $query .= $this -> addWhere('uName', $checkID);
            $data = $this -> db_load($query, 1);
            if(count($data) == 0) {
                return false; // 데이터가 없으면 거짓
            } else {
                return true; // 데이터가 있으면 참
            }
        }
        function music_list($mailAddr, $favorite) {
            $query = $this -> selectAll($mailAddr.' A, '.$mailAddr.'_playlist B', 'A.*');
            $query .= $this -> addWhereLike('B.favorite', $favorite);
            $query .= ' AND A.SID = B.songID';
            $data = $this -> db_load($query, 1);
            return $data;
            //$query = $this -> selectAll($mailAddr);
            //$data = $this -> db_load($query, 1);
            //return $data;
        }
        function music_associatList($mailAddr, $assocColumn, $assocValue) {
            $query = $this -> selectAll($mailAddr);
            $query .= $this -> addWhereLike($assocColumn, $assocValue);
            $data = $this -> db_load($query, 1);
            return $data;
        }
        function signUp_mkdir($uName) {
            // 가입 - 폴더 생성 / 파일 복사 // 로컬에서만 사용가능한 코드
/*	        $command = 'xcopy "E:\FTP\Users\NEW" "E:\FTP\Users\\'.$uName.'\*.*" /e /h /k /y';
	        $command = escapeshellcmd($command);
	        $return_var = exec($command);
*/
            // 경로 생성
            $dirArr = array('covers', 'covers/sample', 'lyrics', 'musics', 'original_file',  'upload');
            $fileArr = array('upload/anytomp3.bat' , 'covers/sample/sample.jpg');
            $remote_file = $uName.'/WebPlayer/';
            // 주소 지정
            $conn_id = ftp_connect('osi.iptime.org');
            // 로그인
            $login_result = ftp_login($conn_id, 'ftpUserAdmin', 'xordlf123!@#');
            // 패시브 모드로 연결
            ftp_pasv($conn_id, true);
            // 폴더 생성
            foreach($dirArr as $dir) {
                if (!ftp_mkdir($conn_id, $uName.'/WebPlayer/'.$dir)) {
                    return -1;
                }
            }
            // 파일 업로드
            foreach($fileArr as $file) {
                if (!ftp_put($conn_id, $remote_file.$file, $_SERVER['DOCUMENT_ROOT'].'/public/etc/NEW/WebPlayer/'.$file, FTP_BINARY)) {
                    return -1;
                }
            }
            // ftp 연결 종료
            ftp_close($conn_id);

            return 0;
        }
        function signUp($uEmail, $uName, $uPasswd){
            //중복 체크 
            $data = $this -> checkID($uName);
            if($data){
                return '중복';
            }

            // 가입 - 폴더 생성 / 파일 복사
            if($this -> signUp_mkdir($uName) == -1) {
                return '폴더 생성 실패';
            }
            
            // 데이터 베이스에 정보 등록
            $insertParams = array(
                'uEmail' => $uEmail,
                'uName' => $uName,
                'uPasswd' => sha1($uPasswd)
            );
            $query = $this -> insert_data('memver.memvers', $insertParams);
            $data = $this -> db_load($query, 0);
            return $data;
        }
    }

	//$obj = new model_audio();
	//$obj -> checkID('osi');
?>