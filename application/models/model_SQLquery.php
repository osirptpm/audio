<?php
class model_SQLQuery {
    private $params = array();
    //protected function db_load($query) {
    protected function db_load($query, $existReturnVal) {
        try {
        	require_once('connectvars.php');
        	$result = 'success';

        	$db=new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.'',DB_USER,DB_PASSWORD);
        	$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
        	$stmt=$db->prepare($query);
        	$stmt->execute($this -> params);
        	if($existReturnVal) {
            	$result=$stmt->fetchAll(PDO::FETCH_ASSOC);
        	}
        	$db=null;        // 접속닫기
        	return $result;
        }
        catch (PDOException $e) {
        	return $e->getMessage();
        }
    }
	// 파라미터 추가 함수
	protected function paramsArrangement(...$arr) {
		$paramsNum = count($this -> params);
		for($i = 0 ; $i < func_num_args(); $i++) {
			$this -> params[':value'.($paramsNum + $i + 1)] = func_get_arg($i);
		}
	}
	protected function selectAll($tableName, ...$arr) {
		// 파라미터 초기화 
		$this -> params = array();
	    if(empty($arr) || empty($arr[0])) {
	        $column = '*';
	    } else {
	        $column = '';
	        foreach($arr as $thi) {
	            $column .= $thi.', ';
	        }
	        $column = substr($column, -0,-2);
	    }
	    $query = ' SELECT '.$column.' FROM '.$tableName;
	    return $query;
	}
	// row 개수 구하기
	protected function selectCount($tableName, ...$arr) {
	    if(empty($arr)) {
	        $column = '*';
	    } else $column = $arr[0];
	    $query = 'SELECT count('.$column.') FROM '.$tableName;
	    return $query;
	}
	// where 절
	protected function addWhere($column, $value, ...$arr) {// arr 는 and, or
		// 파라미터 추가
		$this -> paramsArrangement($value);
		if (func_num_args() <= 2) {
			$addStr = ' WHERE '.$column.' = :value'.count($this -> params);
		} else {
			$addStr = ' '.func_get_arg(2).' '.$column.' = :value'.count($this -> params);
		}
		return $addStr;
	}
	// where like 절
	protected function addWhereLike($column, $value, ...$arr) { // arr 는 and, or
		// 파라미터 추가
		$this -> paramsArrangement('%'.$value.'%');
		if (func_num_args() <= 2) {
			$addStr = ' WHERE '.$column.' LIKE :value'.count($this -> params);
		} else {
			$addStr = ' '.func_get_arg(2).' '.$column.' LIKE :value'.count($this -> params);
		}
		return $addStr;
	}
	// 'Limit 20' 문자열 반환, 갯수와 시작 지점 설정 가능
	protected function addLimit(...$arr) {
	    $addStr = " LIMIT 20";
        if(count($arr) > 0){
            $countRow = $arr[0];
            if(count($arr) > 1) $startRow = $arr[1];
            else $startRow = 0;
            $addStr = ' LIMIT '.$countRow.' OFFSET '.$startRow;
        }
	    return $addStr;
	}
	// 정렬 함수 인자가 3개이면 ', 칼럼 정렬순서'
	protected function addSort($sortColumn ,$sortBy, ...$arr) {
	    if(is_numeric($sortBy)) {
            if($sortBy == 0) $sortBy = 'ASC';
            else if($sortBy == 1) $sortBy = 'DESC';
	    }
	    if(count($arr)) {
	        $addStr = ', '.$sortColumn.' '.$sortBy;
	        return $addStr;
	    }
	    $addStr = ' ORDER BY '.$sortColumn.' '.$sortBy;
	    return $addStr;
	}

	// 데이터 삽입
	protected function insert_data($tableName, $arr) {
		// 파라미터 초기화 
		$this -> params = array();
		// 파라미터 추가
		foreach ($arr as $value) {
			$this -> paramsArrangement($value);
		}
		$columns ='';
		$values = '';
		foreach (array_keys($arr) as $column) {
			$columns .= $column.', ';
		}
		foreach (array_keys($this -> params) as $value) {
			$values .= $value.', ';
		}
		$columns = substr($columns, 0, -2);
		$values = substr($values, 0, -2);
		$query = 'INSERT INTO '.$tableName.' ('.$columns.') VALUES ('.$values.')';
		return $query;
	}
}
	/*$obj = new model_SQLQuery();
    echo 'test<br>';
	$data = $obj -> prepare_insert_data('uEmail', 'uName', 'test@naver.com', 'test');
	print_r($data);
	//$data = $obj -> insert_data('osi.memvers', 'signUp', 'osi', $data[0], $data[1], $data[2]);
	$data = $obj -> insert_data('osi.memvers', 'signUp', 'osi', 'uEmail', 'uName', 'test@naver.com', 'test');
    print_r($data);*/
?>