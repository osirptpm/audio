<?php
	class controller_audio {
	    private $model_audio;

		function __construct() {
		    $this -> model_audio = new model_audio();
		}
		function request_first($mailAddr){
		    if ($this -> request_checkID($mailAddr)) {
				session_start();
				$_SESSION['mailAddr'] = $mailAddr;
			}
		    $data['allList'] = $this -> request_list($mailAddr, 'all');
			$assocColumn = "artist";
			$assocValue = $data['allList'][0]['artist'];
			$data["associageList"] = $this -> request_associatList($mailAddr, $assocColumn, $assocValue);
		    return $data;
        }
		function request_checkID($checkID){
		    $data = $this -> model_audio -> checkID($checkID);
		    return $data;
        }
		function request_list($mailAddr, $favorite){
		    $data = $this -> model_audio -> music_list($mailAddr, $favorite);
		    return $data;
        }
		function request_associatList($mailAddr, $assocColumn, $assocValue){
		    $data = $this -> model_audio -> music_associatList($mailAddr, $assocColumn, $assocValue);
		    return $data;
        }
		function request_signUp($uEmail, $uName, $uPasswd){
		    $data = $this -> model_audio -> signUp($uEmail, $uName, $uPasswd);
		    return $data;
        }
	}
?>
