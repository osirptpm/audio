<?php
	class Controller {

		function __construct() {
            require_once $_SERVER['DOCUMENT_ROOT'].'/application/models/Model.php';
		    $data = $this -> request_get();
            /*$isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) || (isset($_SERVER['HTTP_ORIGIN']) && $_SERVER['REQUEST_METHOD'] == 'GET')*/
            if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])) { // ajax 일때
                $this -> request_ajax($data);
            } else { // ajax 아닐때
                $this -> board_load($data);
            }
        }
		function request_get() {
		    if(isset($_GET['mailAddr'])) {
                require_once $_SERVER['DOCUMENT_ROOT'].'/application/controllers/controller_audio.php';
                $controller_audio = new controller_audio();
                $data = $controller_audio -> request_first($_GET['mailAddr']);
                return $data;
            }
		}
		function request_ajax($data) {
            //_DISABLED = 0
            //_NONE = 1
            //_ACTIVE = 2
            if (session_status() != 2) {
                session_start();
            }
		    if(isset($_GET['mailAddr'])) {
				//require_once $_SERVER['DOCUMENT_ROOT'].'/application/views/view_backgroundDiv.php';
			    //require_once $_SERVER['DOCUMENT_ROOT'].'/application/views/view_player.php';
			}
		    if(isset($_POST['Name']) && isset($_POST['Email']) && isset($_POST['Passwd'])) {
                require_once $_SERVER['DOCUMENT_ROOT'].'/application/controllers/controller_audio.php';
                $controller_audio = new controller_audio();
                $data = $controller_audio -> request_signUp($_POST['Email'], $_POST['Name'], $_POST['Passwd']);
                echo $data;
			}
		    if(isset($_GET['checkID'])) {
                require_once $_SERVER['DOCUMENT_ROOT'].'/application/controllers/controller_audio.php';
                $controller_audio = new controller_audio();
                $data = $controller_audio -> request_checkID($_GET['checkID']);
                print_r($data);
			}

		    if(isset($_GET['assocColumn']) && isset($_GET['assocValue'])) {
                require_once $_SERVER['DOCUMENT_ROOT'].'/application/controllers/controller_audio.php';
                $controller_audio = new controller_audio();
                $data = $controller_audio -> request_associatList($_SESSION['mailAddr'], $_GET['assocColumn'], $_GET['assocValue']);
                $arr = $data;
                require_once $_SERVER['DOCUMENT_ROOT'].'/application/views/view_audioLi.php';
			}
		    if(isset($_GET['favorite'])) {
                require_once $_SERVER['DOCUMENT_ROOT'].'/application/controllers/controller_audio.php';
                $controller_audio = new controller_audio();
                $data = $controller_audio -> request_list($_SESSION['mailAddr'], $_GET['favorite']);
                $arr = $data;
                require_once $_SERVER['DOCUMENT_ROOT'].'/application/views/view_audioLi.php';
			}
		}
		function board_load($data) {
			require_once $_SERVER['DOCUMENT_ROOT'].'/application/views/view_board.php';
		}
	}
	$controller = new Controller();
?>
