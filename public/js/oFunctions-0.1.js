// 딜레이 실행 함수
//(타이머로 쓰일 number, 콜백함수, 딜레이 시간(ms))
function DelayTimeout(timer, func, delayTime) {
    clearTimeout(timer);
    timer = setTimeout(func, delayTime);
    return timer;
}
// 페이지 당 함수 한번 실행\
// 객체 생성 후 var PIEFO = new PageInExecuteFuncOnce();
// 스크롤 이벤트에 setAllData(document, window) 넣기
// 스크롤 이벤트에 pageInFunc(동작할 페이지 숫자 or "END", 동작함 함수, 해당 페이지가 아닐때 동작할 함수) 넣기
// 전체 페이지 갯수 확인 함수 PIEFO.currentScrollTop;
function PageInExecuteFuncOnce() {
    var cur_scrollTop, // 현재 스크롤 값
        cur_scrollHeight, // 페이지 전체 세로 길이
        cur_clientHeight, // 보여지는 부분의 높이
        total_pageNum, // 전체 페이지 수
        temp_total_pageNum = -1, // 전체 페이지 수 임시 저장
        cur_pageNum, // 현재 페이지
        end_page = false, // 페이지 맨 끝인지 아닌지
        resetPageNumArray = true,
        executedFuncInPageNumArray;

    // 페이지 관련 값 리턴 함수 getter
    this.__defineGetter__("currentScrollTop",function () {
        return cur_scrollTop;
    });
    this.__defineGetter__("currentScrollHeight",function () {
        return cur_scrollHeight;
    });
    this.__defineGetter__("currentClientHeight",function () {
        return cur_clientHeight;
    });
    this.__defineGetter__("totalPageNum",function () {
        return total_pageNum;
    });
    this.__defineGetter__("currentPageNum",function () {
        return cur_pageNum;
    });
    this.__defineGetter__("endPage",function () {
        return end_page;
    });
    /*
    this.getTotalPage = function () {
        return total_pageNum;
    }
    this.getCurrentPage = function () {
        return cur_pageNum;
    }
    this.isScrollEnd = function () {
        return end_page;
    }*/

    /*// 스크롤 관련 데이터 셋팅
    this.setCurScrollTop = function (currentScrollTop) {
        _setCurScrollTop(currentScrollTop);
    }
    this.setCurScrollHeight = function (currentScrollHeight) {
        _setCurScrollHeight(currentScrollHeight);
    }
    this.setCurrentScrollData = function (currentScrollTop, currentScrollHeight) {
        _setCurrentScrollData(currentScrollTop, currentScrollHeight);
    }*/
    this.setAllData = function (documentObj, windowObj) {
        _setAllData(documentObj, windowObj);
    }
    this.pageInFunc = function (pageNum, func, func2) {
        _pageInFunc(pageNum, func, func2);
    }

    /*// 스크롤 관련 데이터 셋팅
    function _setCurScrollTop(currentScrollTop) {
        cur_scrollTop = currentScrollTop;
    }
    // 스크롤 관련 데이터 셋팅
    function _setCurScrollHeight(currentScrollHeight) {
        cur_scrollHeight = currentScrollHeight;
    }
    // 스크롤 관련 데이터 셋팅
    function _setCurrentScrollData(currentScrollTop, currentScrollHeight, cur_clientHeight) {
        cur_scrollTop = currentScrollTop;
        cur_scrollHeight = currentScrollHeight;
        cur_clientHeight = window.innerHeight;
    }*/

    // 스크롤 시 실행 함수
    function _setAllData(documentObj, windowObj) {
        // 스크롤 시 값 재조정
        cur_scrollTop = documentObj.body.scrollTop || documentObj.documentElement.scrollTop, // 현재 스크롤 값
        cur_scrollHeight = documentObj.body.scrollHeight, // 페이지 전체 세로 길이
        cur_clientHeight = windowObj.innerHeight, // 보여지는 부분의 높이
        total_pageNum = parseInt(cur_scrollHeight/cur_clientHeight), // 전체 페이지 수
        cur_pageNum = parseInt(cur_scrollTop/cur_clientHeight) + 1; // 현재 페이지

        // 스크롤 바닥일 때
        if(cur_scrollTop == cur_scrollHeight - cur_clientHeight) {
            end_page = true;
        } else {
            end_page = false;
        }
        // 총 페이지수가 바뀌면 true, 아니면 false
        if(total_pageNum != temp_total_pageNum) {
            resetArray = true;
        }
        // resetArray = true 일 때(총 페이지수가 바뀌면), 배열 길이 재조정, 0으로 초기화
        if(resetPageNumArray) {
            executedFuncInPageNumArray = new Array(total_pageNum);
            executedFuncInPageNumArray.fill(-1); // -1인 이유는 스크롤 되있는 상태에서 새로고침시 0일때는 pageInFunc 가 실행이 안됌
            resetPageNumArray = false;
        }
    }
    // 해당 페이지에서 함수 한번만 실행
    function _pageInFunc(pageNum, func, func2) {
        if(pageNum > total_pageNum) {
            //console.log("max values = "+total_pageNum);
            return false;
        }
        if(pageNum == "END" && end_page && executedFuncInPageNumArray[0] != 1) {
            if(typeof func ==  "function") { // 끝일때
                func();
            } else if(typeof func != "undefined") { // func이 함수가 아닌값으로 들어왔을 때
                console.log("func 매개변수가 함수가 아닙니다.");
            }
            executedFuncInPageNumArray[0] = 1;
        } else if(pageNum == "END" && !end_page && executedFuncInPageNumArray[0] != 0){
            if(typeof func2 ==  "function") { // 끝이 아닐때
                func2();
            } else if(typeof func2 != "undefined") { // func2가 함수가 아닌값으로 들어왔을 때
                console.log("func2 매개변수가 함수가 아닙니다.");
            }
            executedFuncInPageNumArray[0] = 0;
        }
        //console.log(executedFuncInPageNumArray);
        if(typeof pageNum != "string" && (cur_pageNum == pageNum) && (executedFuncInPageNumArray[pageNum] != 1)) {
            if(typeof func ==  "function") { // 해당 페이지 일때
                func();
            } else if(typeof func != "undefined") { // func이 함수가 아닌값으로 들어왔을 때
                console.log("func 매개변수가 함수가 아닙니다.");
            }
            executedFuncInPageNumArray[pageNum] = 1;
        } else if(typeof pageNum != "string" && cur_pageNum != pageNum && (executedFuncInPageNumArray[pageNum] != 0)) {
            if(typeof func2 ==  "function") { // 해당 페이지가 아닐때
                func2();
            } else if(typeof func2 != "undefined") { // func2가 함수가 아닌값으로 들어왔을 때
                console.log("func2 매개변수가 함수가 아닙니다.");
            }
            executedFuncInPageNumArray[pageNum] = 0;
        }
    }

}
// 원 메뉴 만들기
// var CP = new circleMenu(document.getElementById("circlePlayer"));
// CP.setDegree = 225; // 생성될 메뉴들의 기준점 각도 12시 기준
// CP.createCircleMenu(9); // 메뉴 9개 생성
function circleMenu(parentElement) {
    var parentElement = parentElement, // 생성될 엘리먼트
        ja_circleMenu_controlDiv, // _getCircleDiameter 함수를 위함
        parentCircleDiameter, // parentCircle 의 지름
        childCircleNum = 1, // 인자를 안줄때 기본 생성 자식 개수
        middleDegree = 0, // 중간 childCircle의 각도 12시 기준
        alreadyCreated = false; // resize 같이 창크기 변할때 setChildPosition 함수 쓰기 위함 // 현재 코드에선 필요없음

    // 작은 circle 개수 구하기
    this.__defineGetter__("getChildCircleNumber",function () {
        return childCircleNum;
    });
    // 작은 circle 생성 개수
    this.__defineSetter__("setChildCircleNumber",function (elNum) {
        childCircleNum = elNum;
    });
    // 작은 circle 생성 개수
    this.__defineSetter__("setDegree",function (deg) {
        middleDegree = -deg;
    });

    // circleMenu 의 dom, childCircle 위치 조절
    this.createCircleMenu = function (elNum) {
        // circleMenu 의 dom, stylesheet 생성
        _createCircleMenu(elNum);
    }
    // childCircle 의 위치 조절
    this.setChildPosition = function () {
        // childCircle 의 위치 조절
        _setChildPosition();
    }
    // childCircle 보이기
    this.showChildCircle = function () {
        // childCircle 보이기
        _showChildCircle();
    }
    // childCircle 숨기기
    this.hideChildCircle = function () {
        // childCircle 숨기기
        _hideChildCircle();
    }
    // childCircle 보이기, 숨기기
    this.toggleChildCircle = function () {
        // childCircle 보이기, 숨기기
        _toggleChildCircle();
    }


    // circleMenu 의 dom, stylesheet 생성
    function _createCircleMenu(elNum) {
        if(typeof elNum != "undefined"){
            childCircleNum = elNum;
        }
        var childDivLength = parentElement.getElementsByTagName("div").length;
        if(childDivLength >= 1) {
            var is_circleMenu_playingDiv = false;
            for(var i = 1; i <= childDivLength && !is_circleMenu_playingDiv; i++){
                is_circleMenu_playingDiv = parentElement.childNodes[i].className == "circleMenu_playingDiv";
            }
            if(is_circleMenu_playingDiv){
                ja_circleMenu_controlDiv = document.getElementsByClassName("circleMenu_controlDiv")[0];
                childCircleNum = ja_circleMenu_controlDiv.getElementsByClassName("circleMenu_childCircle").length;
                // childCircle 의 위치 stylesheet 생성
                _setChildPosition();
                return;
            }
        }
        var circleMenu_playingDiv = document.createElement("div"),
            circleMenu_controlDiv = document.createElement("div"),
            circleMenu_albumCover = document.createElement("img");

        circleMenu_playingDiv.className = "circleMenu_playingDiv";
        circleMenu_controlDiv.className = "circleMenu_controlDiv";
        circleMenu_albumCover.className = "circleMenu_albumCover";
        parentElement.appendChild(circleMenu_playingDiv)
            .appendChild(circleMenu_controlDiv)
            .appendChild(circleMenu_albumCover);
        ja_circleMenu_controlDiv = document.getElementsByClassName("circleMenu_controlDiv")[0];
        for(var elNum = 1; elNum <= childCircleNum; elNum++) {
            var elDiv = document.createElement("div");
            //elDiv.id = "childCircle"+elNum;
            elDiv.className = "childCircle"+elNum+" circleMenu_childCircle";
            ja_circleMenu_controlDiv.appendChild(elDiv);
        }
        // childCircle 의 위치 조절
        _setChildPosition();
    }
    // circle 지름 구하기
    function _getCircleDiameter(pDiameter) {
        if(typeof pDiameter != "undefined"){
            parentCircleDiameter = pDiameter;
        } else {
            parentCircleDiameter = ja_circleMenu_controlDiv.offsetWidth;
        }
    }
    // childCircle 의 위치 조절
    function _setChildPosition() {
        // circle 지름 구하기
        _getCircleDiameter();

        for(var elNum = 1; elNum <= childCircleNum; elNum++) {
            var x = parentCircleDiameter / 2 * ( Math.sqrt( 800 ) / 25 ) * Math.cos( ( childCircleNum - ( 2 * elNum - 1 ) ) / 2 * Math.acos( 0.92 ) + Math.PI / 180 * (middleDegree + 90 ) );
            var y = parentCircleDiameter / 2 * ( Math.sqrt( 800 ) / 25 ) * Math.sin( ( childCircleNum - ( 2 * elNum - 1 ) ) / 2 * Math.acos( 0.92 ) + Math.PI / 180 * (middleDegree + 90 ) );
            document.getElementsByClassName("childCircle"+elNum)[0].style.transform = "translate(" + x + "px, " + (-y) + "px)";
            document.getElementsByClassName("childCircle"+elNum)[0].style.transition = "transform .3s " + ( elNum - 1 ) / 20 + "s";
        }
    }
    // stylesheet 선택
    function _selectStyleSheet() {
        var stylesheet;
        for(var i in document.styleSheets) {
            if(document.styleSheets[i].href && document.styleSheets[i].href.indexOf("circleMenu.css") != -1) {
                stylesheet = document.styleSheets[i];
                break;
            }
        }
        return stylesheet;
    }
    // childCircle 보이기
    function _showChildCircle() {
        var childCircle = document.getElementsByClassName("circleMenu_childCircle");
        for(var i=0; i < childCircle.length; i++) {
            childCircle[i].classList.remove("hideChildBtn");
        }
    }
    // childCircle 숨기기
    function _hideChildCircle() {
        var childCircle = document.getElementsByClassName("circleMenu_childCircle");
        for(var i=0; i < childCircle.length; i++) {
            childCircle[i].classList.add("hideChildBtn");
        }
    }
    // childCircle 보이기, 숨기기
    function _toggleChildCircle() {
        var childCircle = document.getElementsByClassName("circleMenu_childCircle");
        for(var i=0; i < childCircle.length; i++) {
            childCircle[i].classList.toggle("hideChildBtn");
        }
    }

}

// 넘겨받은 이미지를 blur 처리 (맘에 안듬)
function CreateBlurImage(ja_canvas, ja_img) {

    'use strict';

    var canvas = ja_canvas,
        cw = canvas.width,
        ch = canvas.height,
        drawPosition = "default",
        canvasCtx = canvas.getContext("2d"),
        blurCanvas,
        blurCtx,
        blurData,
        smallImgWidth,
        blurRate,
        img = ja_img,
        iw,
        ih,
        rSum = 0, gSum = 0, bSum = 0, aSum = 0,
        closePixelArr = new Array(4),
        ready = false;

    this.drawBlurImage = function (sImgWidth, bRate, position) {
        _drawBlurImage(sImgWidth, bRate, position);
    }
    this.resizeNDrawCanvas = function () {
        _resizeNDrawCanvas();
    }

    function _loadData(smallImgWidth) {
        iw = img.width = smallImgWidth;
        ih = img.height = img.naturalHeight * iw / img.naturalWidth;
        blurCanvas = document.createElement("canvas");
        blurCanvas.width = cw;
        blurCanvas.height = ch,
        blurCtx = blurCanvas.getContext("2d");
        blurCtx.drawImage(img, 0, 0, iw, ih);
        blurData = blurCtx.getImageData(0, 0, iw, ih);
    }
    function _blurData(data) {
        var closePixelArr = new Array(4);
        _blurPixel(data, closePixelArr);
    }
    function _blurPixel(data, closePixelArr) {
        var biw = iw * 4;
        for(var e = 0; e < blurRate; e++){
            for(var i = 0; i < data.length; i+=4) {
                rSum = 0, gSum = 0, bSum = 0, aSum = 0;
                closePixelArr = [
                    i - biw,
                    i - 4, i + 4,
                    i + biw
                ];
                for(var p = 0; p < 4; p++) { // 8 = closePixelArr.length
                    if (closePixelArr[p] >= 0 && closePixelArr[p] <= data.length - 3) {
                        rSum += data[closePixelArr[p]];
                        gSum += data[closePixelArr[p] + 1];
                        bSum += data[closePixelArr[p] + 2];
                    }
                }
                data[i] = rSum / 4;
                data[i+1] = gSum / 4;
                data[i+2] = bSum / 4;
                data[i+3] = 255;
            }
        }
    }
    function _drawBlurImage(sImgWidth, bRate, position) {
        cw = canvas.width;
        ch = canvas.height;
        smallImgWidth = sImgWidth || 100; // 기본 이미지 크기 -> 작으면 더 흐려짐
        blurRate = bRate || 4; // 번짐 정도 -> 높으면 더 흐려짐
        drawPosition = position || drawPosition;

        img.onload = function () {
            _loadData(smallImgWidth);
            _blurData(blurData.data, blurRate);
            blurCtx.putImageData(blurData, 0, 0);
            ready = true;
            _resizeNDrawCanvas();
        }

    }
    function _resizeNDrawCanvas() {
        if(ready) {
            var cutPixel = 0.934426+0.01*smallImgWidth+0.221824*blurRate;
            cw = canvas.width;
            ch = canvas.height;

            var src = {x : 0, y : 0, w : cw, h : ch};
            _blurImagePosition(src);
            canvasCtx.drawImage(
                blurCanvas,
                cutPixel, cutPixel,
                iw - cutPixel * 2, ih - cutPixel * 2,
                src.x, src.y,
                src.w, src.h
            );
        }
    }
    function _blurImagePosition(src) {
        if(drawPosition == "default") {
            src.x = 0, src.y = 0, src.w = cw, src.h = src.w * ih / iw;
        } else if(drawPosition == "cover") {
            if(cw > ch) { // 캔버스 가로 길이가 더 길때
                src.w = cw;
                src.h = src.w * ih / iw;
                src.x = 0;
                src.y = ch / 2 - src.h / 2;
            } else { // 캔버스 세로 길이가 더 길때
                src.h = ch;
                src.w = src.h * iw / ih;
                src.x = cw / 2 - src.w / 2;
                src.y = 0;
            }
        }
    }
}
//  > 해시함수처럼 코드 업데이트 해야할지도
//
// history state 정규식으로 GET데이터 조작 함수
//  HSC = new HistoryStateControl(), // history state 정규식으로 GET 데이터 조작 함수
//  HSC.addGetData("UN", mailAddr); // history 주소에 GET 키와 value 설정
function HistoryStateControl() {
    this.__defineGetter__("getLocationSearch", function () {
        return location.search;
    });
    this.__defineSetter__("setLocationSearch", function (val) {
        locationSearch = val;
    });

    this.addGetData = function (key, value, state) {
        _addGetData(key, value, state);
    }
    this.removeGetData = function (key, state) {
        _removeGetData(key, state);
    }
    this.addParam = function (key, value) {
        return _addParam(key, value);
    }
    this.removeParam = function (key) {
        return _removeParam(key);
    }

    function _addGetData(key, value, state) { // 해당 key와 value 추가
        history.pushState(state, null, _addParam(key, value));
    }
    function _removeGetData(key, state) { // 해당 key 와 그 value 제거
        history.pushState(state, null, _removeParam(key));
    }

    // GET 데이터 값 추가
    function _addParam(key, value) {
        var locationSearch = location.search,
            patt = new RegExp(""+key+"=[a-zA-Z0-9]*&?", "g"),
            str;
        //console.log(patt.exec(str));
        if (patt.test(locationSearch)) { // 해당 문자열이 있다면
            str = locationSearch.replace(patt, "");
            // 아무것도 없다면 ?추가로 GET 데이터 표시 // 끝에 &가 없다면 & 추가
            str = (str.length <= 2) ? "?" : (str = (/&$/.test(str)) ? str : str + "&");
            str = str + key + "=" + value;
        } else { // 해당 문자열이 없다면
            // get 데이터가 아예 없는 경우 ? 이미 있는경우 뒤에 &로 붙임
            str = (locationSearch.length <= 2) ? "?" : locationSearch + "&";
            str = str + key + "=" + value;
        }
        return str;
    }
    // GET 데이터에 값 제거
    function _removeParam(key) {
        var locationSearch = location.search,
            //patt = /UN=[a-zA-Z0-9]*&?/g,
            patt = new RegExp(""+key+"=[a-zA-Z0-9]*&?", "g"),
            str;
        //console.log(patt.exec(str));
        if (patt.test(locationSearch)) { // 해당 문자열이 있다면
            str = locationSearch.replace(patt, ""); // 제거
            str = str.replace(/&?$/, ""); // 끝에 &가 남아있다면 제거
        } else { // 해당 문자열이 없다면
            str = locationSearch; // 그대로
        }
        return str;
    }
}
//  > 해시함수처럼 코드 업데이트 해야함
//
//  주소값(GET)에 따른 DOM 컨트롤 함수`
//  var VCBHP = new ViewControlByGetParam(), // 주소값에 따른 DOM 컨트롤 함수
//  var VCBHPLoad = VCBHP.loadChainFunc(); // chainFunc 오브젝트 로드 
//  VCBHPLoad.setKeyNFunc("keyname",function,function)
//      .setKeyNFunc("keyname",function,function); // 메소드 체인 가능
//  VCBHP.onFunc("keyname"); // 설정된 on함수 실행
//  VCBHP.offFunc("keyname"); // 설정된 off함수 실행
//  VCBHP.checkAllFunc(); 주소값에 keyNFunc 오브젝트에 설정된 key 값이 있을때 onFunc함수 실행, 없을때 offFunc함수 실행
function ViewControlByGetParam() {
    var keyNFuncOn = {},
        keyNFuncOff = {};
    var chainFunc = { // 메소드 체인을 위해 오브젝트로 구성
        setKeyNFunc : function (key, func1, func2) {
            keyNFuncOn[key] = function () {
                if(typeof func1 == "function") {
                    func1();
                } else {
                    null;
                }
            };
            keyNFuncOff[key] = function () {
                if(typeof func2 == "function") {
                    func2();
                } else {
                    null;
                }
            };
            return this; // 메소드 체인을 위해 this 리턴
        },
        onFunc : function (key) {
            keyNFuncOn[key]();
            return this; // 메소드 체인을 위해 this 리턴
        },
        offFunc : function (key) {
            keyNFuncOff[key]();
            return this;
        }
    };
    this.loadChainFunc = function () {
        return _loadChainFunc();
    }
    this.checkAllFunc = function () {
        _checkAllFunc();
    }
    function _loadChainFunc() {
        return chainFunc;
    }
    // 현재 주소값에 key값이 있다면 해당 함수 실행
    function _checkAllFunc() {
        var locationSearch = location.search;
        //console.log(locationSearch);
        for (var key in keyNFuncOn) {
            patt = new RegExp(""+key+"=[a-zA-Z0-9]*&?", "g");
            if (patt.test(locationSearch)) {
                keyNFuncOn[key]();
            } else {
                keyNFuncOff[key]();
            }
        }
    }
}


//  hash 배열,문자열함수로 hash 조작 함수
//  HSC = new HashStateControl(), // hash 조작 함수
//  HSC.add("UN"); // hash 주소에 값 설정
function HashStateControl() {
    this.__defineGetter__("getLocationHash", function () {
        return location.hash;
    });
    this.__defineSetter__("setLocationHash", function (val) {
        locationhash = val;
    });

    this.hasHash = function (key) {
        return _hasParam(key);
    }
    this.add = function (key) {
        return _addParam(key);
    }
    this.remove = function (key) {
        return _removeParam(key);
    }
    this.toggle = function (key) {
        return _toggleParam(key);
    }
    this.replace = function (key, value) {
        return _replaceParam(key, value);
    }

    // hash 데이터 확인
    function _hasParam(key) {
        var locationHash = location.hash,
            hashArr = locationHash.split("#");
        //console.log(hashArr);
        //console.log(hashArr.indexOf(key));
        //console.log(hashArr.join("#"));
        if (hashArr.indexOf(key) == -1) { // 없다면
            return false;
        } else { // 있다면
            return true;
        }
    }
    // hash 데이터 값 추가
    function _addParam(key) {
        var bodyScrollTop = document.documentElement.scrollTop || document.body.scrollTop; // 스크롤탑으로 가는거 방지
        //console.log(patt.exec(str));
        var locationHash = location.hash,
            hashArr = locationHash.split("#"),
            str;
        if (hashArr.indexOf(key) == -1) { // 없다면
            str = location.hash = hashArr.join("#") + "#"+ key;
        } else { // 있다면
            str = location.hash = hashArr.join("#");
        }
        document.body.scrollTop = bodyScrollTop; // 스크롤탑으로 가는거 방지
        document.documentElement.scrollTop = bodyScrollTop; // 스크롤탑으로 가는거 방지
        return str;
    }
    // hash 데이터에 값 제거
    function _removeParam(key) {
        var bodyScrollTop = document.documentElement.scrollTop || document.body.scrollTop; // 스크롤탑으로 가는거 방지
        //console.log(patt.exec(str));
        
        var locationHash = location.hash,
            hashArr = locationHash.split("#"),
            str;
        var hashCheck = hashArr.indexOf(key);
        if (hashCheck == -1) { // 없다면
            str = location.hash = hashArr.join("#");
        } else { // 있다면
            hashArr.splice(hashCheck, 1);
            str = location.hash = hashArr.join("#");
        }
        document.body.scrollTop = bodyScrollTop; // 스크롤탑으로 가는거 방지
        document.documentElement.scrollTop = bodyScrollTop; // 스크롤탑으로 가는거 방지
        return str;
    }
    // hash 데이터에 값 토글
    function _toggleParam(key) {
        var bodyScrollTop = document.documentElement.scrollTop || document.body.scrollTop; // 스크롤탑으로 가는거 방지
        
        var locationHash = location.hash,
            hashArr = locationHash.split("#"),
            str;
        var hashCheck = hashArr.indexOf(key);
        if (hashCheck == -1) { // 없다면
            str = location.hash = hashArr.join("#") + "#"+ key; // 만들고
        } else { // 있다면
            hashArr.splice(hashCheck, 1); // 지우고
            str = location.hash = hashArr.join("#");
        }
        document.body.scrollTop = bodyScrollTop; // 스크롤탑으로 가는거 방지
        document.documentElement.scrollTop = bodyScrollTop; // 스크롤탑으로 가는거 방지
        return str;
    }
    // hash 데이터에 값 바꾸기
    function _replaceParam(key, value) {
        var bodyScrollTop = document.documentElement.scrollTop || document.body.scrollTop; // 스크롤탑으로 가는거 방지
        
        var locationHash = location.hash,
            hashArr = locationHash.split("#"),
            str;
        var hashCheck = hashArr.indexOf(key);
        if (hashCheck == -1) { // 없다면
            str = locationHash; // 아무것도 안함
        } else { // 있다면
            hashArr.splice(hashCheck, 1); // 지우고
            str = location.hash = hashArr.join("#") + "#"+ value;
        }
        document.body.scrollTop = bodyScrollTop; // 스크롤탑으로 가는거 방지
        document.documentElement.scrollTop = bodyScrollTop; // 스크롤탑으로 가는거 방지
        return str;
    }
}

// 주소값(hash)에 따른 DOM 컨트롤 함수
//  var VCBHP = new ViewControlByHashParam(), // 주소값에 따른 DOM 컨트롤 함수
//  var VCBHPLoad = VCBHP.loadChainFunc(); // chainFunc 오브젝트 로드 
//  VCBHPLoad.setKeyNFunc("keyname",function,function)
//      .setKeyNFunc("keyname",function,function); // 메소드 체인 가능
//  VCBHPLoad.setKeyNFunc({
//      "keyname": {
//          "on": function, // 해시가 있을때
//          "off": function // 해시가 없을때
//      },
//      "keyname": {
//          "on": function,
//          "off": function
//      }
// }); // 오브젝트로 넘기기 가능
//  VCBHP.onFunc("keyname"); // 설정된 on함수 실행
//  VCBHP.offFunc("keyname"); // 설정된 off함수 실행
//  VCBHP.checkAllFunc(); 주소값에 keyNFunc 오브젝트에 설정된 key 값이 있을때 onFunc함수 실행, 없을때 offFunc함수 실행

function ViewControlByHashParam() {
    var keyNFuncOn = {},
        keyNFuncOff = {};
    var chainFunc = { // 메소드 체인을 위해 오브젝트로 구성
        setKeyNFunc : function (key, func1, func2) {
            if (typeof key == "object") {
                var obj = key,
                    keys = Object.keys(obj);
                for (var i in keys) {
                    var key = keys[i], // 해시값
                        func1 = obj[key].on,  // 해당 함수
                        func2 = obj[key].off;  // 해당 함수
                    
                    keyNFuncOn[key] = (typeof func1 == "function") ? func1 : null;
                    keyNFuncOff[key] = (typeof func2 == "function") ? func2 : null;
                }
            } else {
                keyNFuncOn[key] = (typeof func1 == "function") ? func1 : null;
                keyNFuncOff[key] = (typeof func2 == "function") ? func2 : null;
            }
            return this; // 메소드 체인을 위해 this 리턴
        },
        onFunc : function (key) {
            keyNFuncOn[key]();
            return this; // 메소드 체인을 위해 this 리턴
        },
        offFunc : function (key) {
            keyNFuncOff[key]();
            return this;
        }
    };
    this.loadChainFunc = function () {
        return _loadChainFunc();
    }
    this.checkAllFunc = function () {
        _checkAllFunc();
    }
    function _loadChainFunc() {
        return chainFunc;
    }
    // 현재 주소값에 key값이 있다면 해당 함수 실행
    function _checkAllFunc() {
        var locationHash = location.hash;
        //console.log(locationHash);
        for (var key in keyNFuncOn) {
            patt = new RegExp("#"+key+"#|#"+key+"$", "g");
            if (patt.test(locationHash)) {
                if (typeof keyNFuncOn[key] == "function")
                keyNFuncOn[key]();
            } else {
                if (typeof keyNFuncOff[key] == "function")
                keyNFuncOff[key]();
            }
        }
    }


    /*this.__defineGetter__("getKeyNFuncOn", function () {
        return keyNFuncOn;
    });
    this.__defineGetter__("getKeyNFuncOff", function () {
        return keyNFuncOff;
    });*/

}
// 미디어 리스트 재생 관련 함수
// var MPLC = new MediaPlaylistControl(ja_media, ja_playList);
// ja_playList 의 자식 요소들이 src, type 속성을 가지고 있어야함
// MPLC.first();
// MPLC.next();
// MPLC.prev();
// MPLC.thisTrack(this);
// MPC.getProgress, MPC.getBuffered
function MediaPlaylistControl(ja_media, ja_playList) {
    var ja_mediaEl = ja_media,
        ja_playListEl = ja_playList,
        currentEl = ja_playListEl.firstElementChild;

    var playback = false;

    _loadNplay();
    
    this.__defineGetter__("getTrack", function () {
        return currentEl;
    });
    this.__defineGetter__("getAudioElement", function () {
        return ja_mediaEl;
    });
    this.__defineGetter__("getProgress", function () {
        return ja_mediaEl.currentTime / ja_mediaEl.duration * 100;;
    });
    this.__defineGetter__("getBuffered", function () {
        return (ja_mediaEl.buffered.end(ja_mediaEl.buffered.length - 1)) / ja_mediaEl.duration * 100;
    });

    this.__defineSetter__("setPlayback", function (playBackBoolean) { // 로드와 같이 재생할것인지
        playback = playBackBoolean;
    });

    this.__defineSetter__("setPlaylist", function (ja_playList) { // 플레이 리스트 바꾸기
        ja_playListEl = ja_playList;
        currentEl = ja_playListEl.firstElementChild;
    });

    this.first = function () {
        _first();
    }
    this.next = function () {
        _next();
    }
    this.prev = function () {
        _prev();
    }
    this.thisTrack = function (thisEl) {
        _thisTrack(thisEl);
    }
    this.PP = function () {
        _PP();
    }


    function _first() {
        currentEl = currentEl.parentElement.firstElementChild;
        _loadNplay();
    }
    function _next() {
        if (currentEl.nextElementSibling) {
            currentEl = currentEl.nextElementSibling;
        } else {
            currentEl = currentEl.parentElement.firstElementChild;
        }
        _loadNplay();
    }
    function _prev() {
        if (currentEl.previousElementSibling) {
            currentEl = currentEl.previousElementSibling;
        } else {
            currentEl = currentEl.parentElement.lastElementChild;
        }
        _loadNplay();
    }
    function _thisTrack(thisEl) {
        currentEl = thisEl;
        _loadNplay();
    }
    function _PP() {
		if (ja_mediaEl.paused == true) {
			ja_mediaEl.play();
		} else {
			ja_mediaEl.pause();
		}
    }

    function _loadNplay() {
        // 현재 src 값 제거
        ja_mediaEl.removeAttribute("src");
        ja_mediaEl.removeAttribute("type");
        //선택한 버튼의 미디어 경로를 불러옴
        ja_mediaEl.setAttribute("src", currentEl.getAttribute("src"));
        ja_mediaEl.setAttribute("type", currentEl.getAttribute("type"));
        //미디어 다시 load 함
        ja_mediaEl.load();
        if (playback) {
            ja_mediaEl.play();
        }
        //ja_mediaEl.play();
    }
}

//  터치 스와이프 기능 (아직 싱글터치만)
//  콜백함수로 함수 지정하면 동작
//  var swipe_player_board = new SwipeElement(document.getElementById("player_board")); 또는
//  var swipe_player_board = new SwipeElement(document.getElementById("player_board"), false); false 는 스와이프시 스크롤 같은 다른 이벤트 제거하지 않음
//  swipe_player_board = swipe_player_board.loadChainFunc();
//  swipe_player_board.swipeUp(test, 200, 20) // 200은 스와이프 거리, 기본값은 80, 20은 정확도, 기본값은 30
//      .swipeDown(function () {
//          console.log("down");
//      }, 200) // 200은 스와이프 거리 
//      .swipeRight(function () {
//          console.log("right");
//      });
//  function test() {
//      console.log("up");
//  }
function SwipeElement(swipeEl, preventDefaultBoolean, preventBubbleBoolean) { // 버블은 손가락을때면 한번 함수 실행
    var el = swipeEl,
        swipeStart = [],
        swipeEnd = [],
        moveX = [],
        moveY = [],
        defaultRespon = 80,
        defaultAccuracy = 30,
        setSwipeUpFunc = {func:function () {}, isset: false, respon: defaultRespon, accuracy: defaultAccuracy},
        setSwipeDownFunc = {func:function () {}, isset: false, respon: defaultRespon, accuracy: defaultAccuracy},
        setSwipeLeftFunc = {func:function () {}, isset: false, respon: defaultRespon, accuracy: defaultAccuracy},
        setSwipeRightFunc = {func:function () {}, isset: false, respon: defaultRespon, accuracy: defaultAccuracy},
        preventDefault = (preventDefaultBoolean == false) ? false : true, //기본값 true
        preventBubble = (preventBubbleBoolean == true) ? true : false; // 기본값 false

    var chainFunc = { // 메소드 체인을 위해 오브젝트로 구성
        swipeUp : function (func, respon, accuracy) {
            if (typeof func == "function") {
                setSwipeUpFunc.func = func;
                setSwipeUpFunc.isset = true;
                setSwipeUpFunc.respon = respon || defaultRespon;
                setSwipeUpFunc.accuracy = accuracy || defaultAccuracy;
            } else {
                console.log(func+" is not a function");
            }
            return this; // 메소드 체인을 위해 this 리턴
        },
        swipeDown : function (func, respon, accuracy) {
            if (typeof func == "function") {
                setSwipeDownFunc.func = func;
                setSwipeDownFunc.isset = true;
                setSwipeDownFunc.respon = respon || defaultRespon;
                setSwipeDownFunc.accuracy = accuracy || defaultAccuracy;
            } else {
                console.log(func+" is not a function");
            }
            return this; // 메소드 체인을 위해 this 리턴
        },
        swipeLeft : function (func, respon, accuracy) {
            if (typeof func == "function") {
                setSwipeLeftFunc.func = func;
                setSwipeLeftFunc.isset = true;
                setSwipeLeftFunc.respon = respon || defaultRespon;
                setSwipeLeftFunc.accuracy = accuracy || defaultAccuracy;
            } else {
                console.log(func+" is not a function");
            }
            return this; // 메소드 체인을 위해 this 리턴
        },
        swipeRight : function (func, respon, accuracy) {
            if (typeof func == "function") {
                setSwipeRightFunc.func = func;
                setSwipeRightFunc.isset = true;
                setSwipeRightFunc.respon = respon || defaultRespon;
                setSwipeRightFunc.accuracy = accuracy || defaultAccuracy;
            } else {
                console.log(func+" is not a function");
            }
            return this; // 메소드 체인을 위해 this 리턴
        }
    };

    this.loadChainFunc = function () {
        return _loadChainFunc();
    }

    el.addEventListener("touchstart", _swipeStart);
    el.addEventListener("touchmove", _swipeMove);
    el.addEventListener("touchend", _swipeEnd);

    function _loadChainFunc() {
        return chainFunc;
    }
    function _swipeStart(event) { // event 파이어폭스에서는 해야 사용가능
        var touches = event.touches;
        swipeStart = [];
        swipeEnd = [];

        for (var i = 0; i < touches.length; i++) {
            swipeStart[i] = {x:0, y:0};
            swipeEnd[i] = {x:0, y:0};
            swipeStart[i]["x"] = touches[i].screenX;
            swipeStart[i]["y"] = touches[i].screenY;
            //swipeEnd[i]["x"] = touches[i].screenX;
            //swipeEnd[i]["y"] = touches[i].screenY;
        }
        //console.log("swipeStart");
    }
    function _swipeMove(event) {
        if (preventDefault) {
            event.preventDefault();
        }
        
        var touches = event.touches;

        for (var i = 0; i < touches.length; i++) {
            swipeEnd[i]["x"] = touches[i].screenX;
            swipeEnd[i]["y"] = touches[i].screenY;
            
            moveX[i] = swipeStart[i]["x"] - swipeEnd[i]["x"];
            moveY[i] = swipeStart[i]["y"] - swipeEnd[i]["y"];

            if (!preventBubble) { // 거짓이면 계속 움직일때 반복실행
                _swipeUp(touches, i);
                _swipeDown(touches, i);
                _swipeLeft(touches, i);
                _swipeRight(touches, i);
            }
        }
        //console.log("swipeMove");
        //console.log("X : "+moveX[0]+", Y : "+moveY[0]);
    }
    function _swipeEnd(event) {
        var touches = event.changedTouches;
        if (preventBubble) { // 참이면 계속 움직여도 한번 실행
            for (var i = 0; i < touches.length; i++) {
                _swipeUp(touches, i);
                _swipeDown(touches, i);
                _swipeLeft(touches, i);
                _swipeRight(touches, i);

                moveX[i] = swipeStart[i]["x"] - swipeEnd[i]["x"];
                moveY[i] = swipeStart[i]["y"] - swipeEnd[i]["y"];
            }
        }
        //console.log("swipeEnd");
    }
    
    function _swipeUp(touches, i) {
        if (moveY[i] > setSwipeUpFunc.respon // 기본값 80
                && moveX[i] < setSwipeUpFunc.accuracy // 기본값 30
                && moveX[i] > -setSwipeUpFunc.accuracy) { // 위쪽으로
            setSwipeUpFunc.func();
            // 초기화
            _resetSwipeStart(touches, i);
            //console.log("위쪽으로/"+i);
        }
    }
    function _swipeDown(touches, i) {
        if (moveY[i] < -setSwipeDownFunc.respon // 기본값 80
                && moveX[i] < setSwipeDownFunc.accuracy // 기본값 30
                && moveX[i] > -setSwipeDownFunc.accuracy) { // 아래쪽으로
            setSwipeDownFunc.func();
            // 초기화
            _resetSwipeStart(touches, i);
        }
    }
    function _swipeLeft(touches, i) {
        if (moveX[i] > setSwipeLeftFunc.respon // 기본값 80
                && moveY[i] < setSwipeLeftFunc.accuracy // 기본값 30
                && moveY[i] > -setSwipeLeftFunc.accuracy) { // 왼쪽으로
            setSwipeLeftFunc.func();
            // 초기화
            _resetSwipeStart(touches, i);
        }
    }
    function _swipeRight(touches, i) {
        if (moveX[i] < -setSwipeRightFunc.respon // 기본값 80
                && moveY[i] < setSwipeRightFunc.accuracy // 기본값 30
                && moveY[i] > -setSwipeRightFunc.accuracy) { // 오른쪽으로
            setSwipeRightFunc.func();
            // 초기화
            _resetSwipeStart(touches, i);
        }
    }
    // 초기화
    function _resetSwipeStart(touches, i) {
        swipeStart[i]["x"] = touches[i].screenX;
        swipeStart[i]["y"] = touches[i].screenY;
    }
}
/*function HideScrollBar1(El) {
    var el = El,
        elInnerHtml = el.innerHTML;
    elInnerHtml = "<div>"+elInnerHtml+"</div>";
    el.innerHTML = elInnerHtml;
    el.style.overflow = "hidden";
    var chEl = el.children[0];
    chEl.style.overflowY = "scroll";
    chEl.style.height = "inherit";
    console.log(el.offsetWidth);
    console.log(el.scrollWidth);
    console.log(el.clientWidth);
    console.log(chEl.offsetWidth);
    var scrollBarWidth = chEl.offsetWidth - chEl.scrollWidth;
    chEl.style.width = chEl.offsetWidth + scrollBarWidth + "px";
    console.log(chEl.offsetWidth);
    console.log(scrollBarWidth);
    //el.appendChild()
}*/