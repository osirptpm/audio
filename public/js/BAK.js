
            //var ja_albumArtCD = document.getElementsByClassName("albumArtCD_canvas");
            var ja_albumArtCD = $(".albumArtCD_canvas").get(0);
            var ctx = ja_albumArtCD.getContext("2d");
            var IMG = new Image;
            IMG.src = "/public/images/S/technology-music-sound-things.jpg";
            IMG.src = "http://osi.iptime.org/cloud/osi/WebPlayer/covers/01.%20Kiss%20My%20Lips.jpeg";
            IMG.src = "http://osi.iptime.org/cloud/osi/WebPlayer/covers/01%20Say%20Something.jpeg";
            IMG.width = ja_albumArtCD.offsetWidth;
            IMG.height = ja_albumArtCD.offsetHeight;
            console.log(IMG);
            ja_albumArtCD.width = ja_albumArtCD.offsetWidth;
            ja_albumArtCD.height = ja_albumArtCD.offsetHeight;
            ctx.beginPath();
            ctx.arc(ja_albumArtCD.width/2,ja_albumArtCD.height/2,ja_albumArtCD.height/2,0,2*Math.PI);
            ctx.strokeStyle="rgba(0,0,0,0.5)"
            ctx.lineWidth = 14;
            ctx.clip();
            IMG.onload = function () {
                ctx.drawImage(IMG,ja_albumArtCD.width/2 - IMG.width/2,ja_albumArtCD.height/2 - IMG.height/2,IMG.width,IMG.width);
                ctx.stroke();
      /*ctx.shadowColor = "black";
      ctx.shadowOffsetX = 0;
      ctx.shadowOffsetY = 0;
      ctx.shadowBlur = "10";
                             ctx.beginPath();
                             ctx.globalCompositeOperation = 'destination-in';
                             ctx.arc(ja_albumArtCD.width/2,ja_albumArtCD.height/2,ja_albumArtCD.height/2,0,2*Math.PI);
                             ctx.fillStyle = '#fff'; //color doesn't matter, but we want full opacity
                             ctx.fill();*/
                ctx.beginPath();
                ctx.globalCompositeOperation = "source-over";
                ctx.arc(ja_albumArtCD.width/2,ja_albumArtCD.height/2,(ja_albumArtCD.width*31.5/100)/2-5,0,2*Math.PI);
                ctx.strokeStyle="rgba(0,0,0,0.5)"
                ctx.lineWidth = 7;
                ctx.stroke();
                ctx.beginPath();
                ctx.globalCompositeOperation = "xor";
                ctx.arc(ja_albumArtCD.width/2,ja_albumArtCD.height/2,(ja_albumArtCD.width*27.5/100)/2,0,2*Math.PI);
                ctx.fillStyle = "rgba(127,127,127,0.5)";
                ctx.fill();
                ctx.beginPath();
                ctx.arc(ja_albumArtCD.width/2,ja_albumArtCD.height/2,(ja_albumArtCD.width*15/100)/2,0,2*Math.PI);
                ctx.clip();
                ctx.clearRect(ja_albumArtCD.width/2 - (ja_albumArtCD.width*15/100)/2,ja_albumArtCD.height/2 - (ja_albumArtCD.width*15/100)/2, (ja_albumArtCD.width*15/100)/2 * 2 + 2, (ja_albumArtCD.width*15/100)/2 * 2 + 2);
            }


// 원 메뉴 만들기
// var CP = new circleMenu(document.getElementById("circlePlayer"));
// CP.setDegree = 225; // 생성될 메뉴들의 기준점 각도 12시 기준
// CP.createCircleMenu(9); // 메뉴 9개 생성
function circleMenu(parentElement) {
    var parentElement = parentElement, // 생성될 엘리먼트
        ja_circleMenu_controlDiv, // _getCircleDiameter 함수를 위함
        parentCircleDiameter, // parentCircle 의 지름
        childCircleNum = 1, // 인자를 안줄때 기본 생성 자식 개수
        middleDegree = 0, // 중간 childCircle의 각도 12시 기준
        alreadyCreated = false; // resize 같이 창크기 변할때 setChildPosition 함수 쓰기 위함

    // 작은 circle 개수 구하기
    this.__defineGetter__("getChildCircleNumber",function () {
        return childCircleNum;
    });
    // 작은 circle 생성 개수
    this.__defineSetter__("setChildCircleNumber",function (elNum) {
        childCircleNum = elNum;
    });
    // 작은 circle 생성 개수
    this.__defineSetter__("setDegree",function (deg) {
        middleDegree = -deg;
    });

    // circleMenu 의 dom, stylesheet 생성
    this.createCircleMenu = function (elNum) {
        // circleMenu 의 dom, stylesheet 생성
        _createCircleMenu(elNum);
    }
    // childCircle 의 위치 stylesheet 생성
    this.setChildPosition = function () {
        // childCircle 의 위치 stylesheet 생성
        _setChildPosition();
    }
    // childCircle 보이기
    this.showChildCircle = function () {
        // childCircle 보이기
        _showChildCircle();
    }
    // childCircle 숨기기
    this.hideChildCircle = function () {
        // childCircle 숨기기
        _hideChildCircle();
    }
    // childCircle 숨기기, 보이기
    this.toggleChildCircle = function () {
        // childCircle 숨기기, 보이기
        _toggleChildCircle();
    }


    // circleMenu 의 dom, stylesheet 생성
    function _createCircleMenu(elNum) {
        if(typeof elNum != "undefined"){
            childCircleNum = elNum;
        }
        if(parentElement.getElementsByTagName("div").length >= 1 && parentElement.childNodes[1].className == "circleMenu_playingDiv"){
            ja_circleMenu_controlDiv = document.getElementsByClassName("circleMenu_controlDiv")[0];
            childCircleNum = ja_circleMenu_controlDiv.getElementsByTagName("div").length;
            // childCircle 의 위치 stylesheet 생성
            _setChildPosition();
            return;
        }
        var circleMenu_playingDiv = document.createElement("div"),
            circleMenu_controlDiv = document.createElement("div"),
            circleMenu_albumCover = document.createElement("img");

        circleMenu_playingDiv.className = "circleMenu_playingDiv";
        circleMenu_controlDiv.className = "circleMenu_controlDiv";
        circleMenu_albumCover.className = "circleMenu_albumCover";
        parentElement.appendChild(circleMenu_playingDiv)
            .appendChild(circleMenu_controlDiv)
            .appendChild(circleMenu_albumCover);
        ja_circleMenu_controlDiv = document.getElementsByClassName("circleMenu_controlDiv")[0];
        for(var elNum = 1; elNum <= childCircleNum; elNum++) {
            var elDiv = document.createElement("div");
            //elDiv.id = "childCircle"+elNum;
            elDiv.className = "childCircle"+elNum+" circleMenu_childCircle";
            ja_circleMenu_controlDiv.appendChild(elDiv);
        }
        // childCircle 의 위치 stylesheet 생성
        _setChildPosition();
    }
    // circle 지름 구하기
    function _getCircleDiameter(pDiameter) {
        if(typeof pDiameter != "undefined"){
            parentCircleDiameter = pDiameter;
        } else {
            parentCircleDiameter = ja_circleMenu_controlDiv.offsetWidth;
        }
    }
    // childCircle 의 위치 stylesheet 생성
    function _setChildPosition() {
        // circle 지름 구하기
        _getCircleDiameter();
        var stylesheet =  _selectStyleSheet();
        var insertIndex =  stylesheet.cssRules.length - 12;

        if(alreadyCreated) {
            for(var elNum = stylesheet.cssRules.length - 1; elNum >= 0; elNum--) {
                if(typeof stylesheet.cssRules[elNum].selectorText == "string" && stylesheet.cssRules[elNum].selectorText.indexOf(".childCircle") != -1) {
                    stylesheet.deleteRule(elNum);
                }
            }
        }
        for(var elNum = 1; elNum <= childCircleNum; elNum++) {
            var x = parentCircleDiameter / 2 * ( Math.sqrt( 800 ) / 25 ) * Math.cos( ( childCircleNum - ( 2 * elNum - 1 ) ) / 2 * Math.acos( 0.92 ) + Math.PI / 180 * (middleDegree + 90 ) );
            var y = parentCircleDiameter / 2 * ( Math.sqrt( 800 ) / 25 ) * Math.sin( ( childCircleNum - ( 2 * elNum - 1 ) ) / 2 * Math.acos( 0.92 ) + Math.PI / 180 * (middleDegree + 90 ) );
            //stylesheet.insertRule(".childCircle" + elNum + " { transform: translate(" + x + "px, " + (-y) + "px); transition: transform .3s " + ( elNum - 1 ) / 20 + "s; }", insertIndex + elNum);

        }
        alreadyCreated = true;
    }
    // stylesheet 선택
    function _selectStyleSheet() {
        var stylesheet;
        for(var i in document.styleSheets) {
            if(document.styleSheets[i].href && document.styleSheets[i].href.indexOf("circleMenu.css") != -1) {
                stylesheet = document.styleSheets[i];
                break;
            }
        }
        return stylesheet;
    }
    // childCircle 보이기
    function _showChildCircle() {
        var childCircle = document.getElementsByClassName("circleMenu_childCircle");
        for(var i=0; i < childCircle.length; i++) {
            removeClass(childCircle[i], "hideChildBtn");
        }
    }
    // childCircle 숨기기
    function _hideChildCircle() {
        var childCircle = document.getElementsByClassName("circleMenu_childCircle");
        for(var i=0; i < childCircle.length; i++) {
            addClass(childCircle[i], "hideChildBtn");
        }
    }
    // childCircle 보이기, 숨기기
    function _toggleChildCircle() {
        var childCircle = document.getElementsByClassName("circleMenu_childCircle");
        for(var i=0; i < childCircle.length; i++) {
            toggleClass(childCircle[i], "hideChildBtn");
        }
    }

}


function blurImage(ja_canvas, ja_img, blurRate) {
    var canvas = ja_canvas,
        img = ja_img,
        cw = canvas.width,
        ch = canvas.height,
        ctx = canvas.getContext("2d"),
        rSum = gSum = bSum = aSum = 0,
        closePixelArr = new Array(4);

    //img.crossOrigin = "Anonymous";
    //console.log(img.crossOrigin);
    var tmpCanvas = document.createElement('canvas');

    img.onload = function() {
        /*var iw = img.naturalWidth,
        ih = img.naturalHeight,*/
        console.log(img.width+"/"+img.height);
        var iw = img.width = 300,
        ih = img.height = img.naturalHeight * iw / img.naturalWidth,
        biw = iw * 4;
        console.log(iw+"/"+ih);

        tmpCanvas.width = iw;
        tmpCanvas.height = ih;
        var tmpCtx = canvas.getContext("2d");

        //console.log(Math.max(0, ((Math.log(22222222) / Math.log(2)) - 3)));

        //ctx.drawImage(img, 0, 0, iw, ih);
        tmpCtx.drawImage(img, 0, 0, iw, ih);
        //var imgData = ctx.getImageData(0,0,iw,ih);
        var imgData = tmpCtx.getImageData(0,0,iw,ih);
        var data = imgData.data;
        for(var b = 0; b < blurRate; b++) {
            for(var i=0;i<imgData.data.length;i+=4) {
                rSum = 0;
                gSum = 0;
                bSum = 0;
                aSum = 0;
                /*closePixelArr = [
                    i - biw - 4, i - biw, i - biw + 4,
                    i - 4, i + 4,
                    i + biw - 4, i + biw, i + biw + 4
                ];*/
                closePixelArr = [
                    i - biw,
                    i - 4, i + 4,
                    i + biw
                ];
                for(var p = 0; p < 4; p++) { // 8 = closePixelArr.length
                    if (closePixelArr[p] >= 0 && closePixelArr[p] <= data.length - 3) {
                        rSum += data[closePixelArr[p]];
                        gSum += data[closePixelArr[p] + 1];
                        bSum += data[closePixelArr[p] + 2];
                        //aSum += data[closePixelArr[p] + 3];
                    }
                }
                data[i] = rSum / 4;
                data[i+1] = gSum / 4;
                data[i+2] = bSum / 4;
                //data[i+3] = aSum / 4; // 8 = closePixelArr.length
            }
        }
        console.log(tmpCtx);
        tmpCtx.putImageData(imgData, 0, 0);
        tmpCtx.drawImage(tmpCanvas, 0, 0, iw, ih, 0, 0, cw, ((cw * ih) / iw));
        //ctx.drawImage(tmpCanvas, 0, 0, iw, ih, 0, 0, cw, ((cw * ih) / iw));
        console.log(cw);
        ctx.drawImage(tmpCanvas, 0, 0, cw, ((cw * ih) / iw), 0, 0, cw, ((cw * ih) / iw));
    }
}


    function _resizeImg() {
        var windowMaxInner = Math.max(window.innerWidth, window.innerHeight);
        if(windowMaxInner > 1025) { // 화면 크리가 클때
            if(img.width >= img.height) { // 이미지 가로 길이가 더 길거나 같을때
                iw = img.width = 400;
                ih = img.height = img.naturalHeight * iw / img.naturalWidth;
            } else { // 이미지 세로 길이가 더 길때
                ih = img.height = 400;
                iw = img.width = img.naturalWidth * ih / img.naturalHeight;
            }
        } else { // 화면 가로길이 1024 이하
            if(img.width >= img.height) { // 이미지 가로 길이가 더 길거나 같을때
                iw = img.width = 100;
                ih = img.height = img.naturalHeight * iw / img.naturalWidth;
            } else { // 이미지 세로 길이가 더 길때
                ih = img.height = 100;
                iw = img.width = img.naturalWidth * ih / img.naturalHeight;
            }
        }
    }

function CreateBlurImage(ja_canvas, ja_img) {

    'use strict';

    var canvas = ja_canvas,
        cw = canvas.width,
        ch = canvas.height,
        drawPosition = "default",
        canvasCtx = canvas.getContext("2d"),
        blurCanvas,
        blurCtx,
        blurData,
        smallImgWidth,
        blurRate,
        img = ja_img,
        iw,
        ih,
        rSum = 0, gSum = 0, bSum = 0, aSum = 0,
        closePixelArr = new Array(4),
        ready = false;

    this.drawBlurImage = function (sImgWidth, bRate) {
        _drawBlurImage(sImgWidth, bRate);
    }
    this.resizeNDrawCanvas = function () {
        _resizeNDrawCanvas();
    }

    function _loadData(smallImgWidth) {
        iw = img.width = smallImgWidth;
        ih = img.height = img.naturalHeight * iw / img.naturalWidth;
        blurCanvas = document.createElement("canvas");
        blurCanvas.width = cw;
        blurCanvas.height = ch,
        blurCtx = blurCanvas.getContext("2d");
        blurCtx.drawImage(img, 0, 0, iw, ih);
        blurData = blurCtx.getImageData(0, 0, iw, ih);
    }
    function _blurData(data) {
        var closePixelArr = new Array(4);
        _blurPixel(data, closePixelArr);

        //var tmpArr = new Uint8Array(iw * ih * 4);
        //var tmpArr = new Array(iw * ih * 4);
        //tmpArr.fill(0);
        //_blurTopPixel(data, tmpArr, blurRate);
        //_blurRightPixel(data, tmpArr, blurRate);
        //_blurBottomPixel(data, tmpArr, blurRate);
        //_blurLeftPixel(data, tmpArr, blurRate);
        //for(var a = 0; a < data.length; a++) {
        //    data[a] = tmpArr[a];
        //}
        //for(var i = 0; i < data.length - 3; i+=4) {
        //    data[i+3] = 255;
        //}
    }
    function _blurPixel(data, closePixelArr) {
        var biw = iw * 4;
        for(var e = 0; e < blurRate; e++){
            for(var i = 0; i < data.length; i+=4) {
                rSum = 0, gSum = 0, bSum = 0, aSum = 0;
                closePixelArr = [
                    i - biw,
                    i - 4, i + 4,
                    i + biw
                ];
                for(var p = 0; p < 4; p++) { // 8 = closePixelArr.length
                    if (closePixelArr[p] >= 0 && closePixelArr[p] <= data.length - 3) {
                        rSum += data[closePixelArr[p]];
                        gSum += data[closePixelArr[p] + 1];
                        bSum += data[closePixelArr[p] + 2];
                    }
                }
                data[i] = rSum / 4;
                data[i+1] = gSum / 4;
                data[i+2] = bSum / 4;
                data[i+3] = 255;
            }
        }
    }

    function _blurTopPixel(data, tmpArr, blurRate) {
        // 위쪽 픽셀
        var biw = iw * 4; // 4줄
        //for(var e = 0; e < blurRate; e++){
            for(var i = (biw * 4); i < data.length - 3; i+=4) {
                if(i <= data.length - (biw * 4)) { // 마지막 4줄 빼고 다
                    tmpArr[i] += data[i - (biw * 4)] / 4;
                    tmpArr[i+1] += data[i - (biw * 4) + 1] / 4;
                    tmpArr[i+2] += data[i - (biw * 4) + 2] / 4;
                    tmpArr[i+3] += 255 / 4;
                } else { // 마지막 4줄
                    tmpArr[i] += data[i - (biw * 4)] / 3;
                    tmpArr[i+1] += data[i - (biw * 4) + 1] / 3;
                    tmpArr[i+2] += data[i - (biw * 4) + 2] / 3;
                    tmpArr[i+3] += 255 / 3;
                }
            }
        //}
        //return tmpArr;
    }
    function _blurRightPixel(data, tmpArr, blurRate) {
        // 오른쪽 픽셀
        var biw = iw * 4; // 4줄
        //for(var e = 0; e < blurRate; e++){
            for(var i = 0; i < data.length - 3; i+=4) {
                if(i % (iw * 4) < 384) { // 오른쪽 4픽셀 빼고 다
                    if(i % (iw * 4) > 15) {
                        tmpArr[i] += data[i + 4] / 4;
                        tmpArr[i+1] += data[i + 4 + 1] / 4;
                        tmpArr[i+2] += data[i + 4 + 2] / 4;
                        tmpArr[i+3] += 255 / 4;
                    } else {
                        tmpArr[i] += data[i + 4] / 3;
                        tmpArr[i+1] += data[i + 4 + 1] / 3;
                        tmpArr[i+2] += data[i + 4 + 2] / 3;
                        tmpArr[i+3] += 255 / 3;
                    }
                }
            }
        //}
        //return tmpArr;
    }
    function _blurBottomPixel(data, tmpArr, blurRate) {
        // 아래쪽 픽셀
        var biw = iw * 4; // 4줄
        //for(var e = 0; e < blurRate; e++){
            for(var i = 0; i < data.length - 3 - (biw * 4); i+=4) {
                if(i >= (biw * 4)) { // 앞의 4줄 빼고 다
                    tmpArr[i] += data[i + (biw * 4)] / 4;
                    tmpArr[i+1] += data[i + (biw * 4) + 1] / 4;
                    tmpArr[i+2] += data[i + (biw * 4) + 2] / 4;
                    tmpArr[i+3] += 255 / 4;
                } else { // 앞의 4줄
                    tmpArr[i] += data[i + (biw * 4)] / 3;
                    tmpArr[i+1] += data[i + (biw * 4) + 1] / 3;
                    tmpArr[i+2] += data[i + (biw * 4) + 2] / 3;
                    tmpArr[i+3] += 255 / 3;
                }
            }
        //}
        //return tmpArr;
    }
    function _blurLeftPixel(data, tmpArr, blurRate) {
        // 왼쪽 픽셀
        var biw = iw * 4; // 4줄
        //for(var e = 0; e < blurRate; e++){
            for(var i = 0; i < data.length - 3; i+=4) {
                if(i % (iw * 4) > 15) { // 왼쪽 4픽셀 빼고 다
                    if(i % (iw * 4) < 84) {
                        tmpArr[i] += data[i - 4] / 4;
                        tmpArr[i+1] += data[i - 4 + 1] / 4;
                        tmpArr[i+2] += data[i - 4 + 2] / 4;
                        tmpArr[i+3] += 255 / 4;
                    } else {
                        tmpArr[i] += data[i - 4] / 3;
                        tmpArr[i+1] += data[i - 4 + 1] / 3;
                        tmpArr[i+2] += data[i - 4 + 2] / 3;
                        tmpArr[i+3] += 255 / 3;
                    }
                }
            }
        //}
        //return tmpArr;
    }

    function _drawBlurImage(sImgWidth, bRate) {
        smallImgWidth = sImgWidth || 100; // 기본 이미지 크기 -> 작으면 더 흐려짐
        blurRate = bRate || 4; // 번짐 정도 -> 높으면 더 흐려짐

        img.onload = function () {
            _loadData(smallImgWidth);
            _blurData(blurData.data, blurRate);
            blurCtx.putImageData(blurData, 0, 0);
            ready = true;
            _resizeNDrawCanvas();
        }

    }
    function _resizeNDrawCanvas() {
        var cutPixel = 0.934426+0.01*smallImgWidth+0.221824*blurRate;
        console.log(cutPixel);
        if(ready) {
            cw = canvas.width;
            ch = canvas.height;
            canvasCtx.drawImage(blurCanvas, cutPixel, cutPixel, iw - cutPixel * 2, ih - cutPixel * 2, 0, 0, cw, ((cw * ih) / iw));
        }
    }
    function _blurImagePosition(position) {
        drawPosition = position || drawPosition;
        if(drawPosition == "cover") {
            if(cw > ch) {

            }
        }
    }
/*
    //img.crossOrigin = "Anonymous";
    //console.log(img.crossOrigin);
    var tmpCanvas = document.createElement('canvas');

    img.onload = function () {
        //var iw = img.naturalWidth,
        //ih = img.naturalHeight,
        console.log(img.width+"/"+img.height);
        var iw = img.width = 300,
        ih = img.height = img.naturalHeight * iw / img.naturalWidth,
        biw = iw * 4;
        console.log(iw+"/"+ih);

        tmpCanvas.width = iw;
        tmpCanvas.height = ih;
        var tmpCtx = canvas.getContext("2d");

        //console.log(Math.max(0, ((Math.log(22222222) / Math.log(2)) - 3)));

        //ctx.drawImage(img, 0, 0, iw, ih);
        tmpCtx.drawImage(img, 0, 0, iw, ih);
        //var imgData = ctx.getImageData(0,0,iw,ih);
        var imgData = tmpCtx.getImageData(0,0,iw,ih);
        var data = imgData.data;
        for(var b = 0; b < blurRate; b++) {
            for(var i=0;i<imgData.data.length;i+=4) {
                rSum = 0;
                gSum = 0;
                bSum = 0;
                aSum = 0;
                //closePixelArr = [
                //    i - biw - 4, i - biw, i - biw + 4,
                //    i - 4, i + 4,
                //    i + biw - 4, i + biw, i + biw + 4
                //];
                closePixelArr = [
                    i - biw,
                    i - 4, i + 4,
                    i + biw
                ];
                for(var p = 0; p < 4; p++) { // 8 = closePixelArr.length
                    if (closePixelArr[p] >= 0 && closePixelArr[p] <= data.length - 3) {
                        rSum += data[closePixelArr[p]];
                        gSum += data[closePixelArr[p] + 1];
                        bSum += data[closePixelArr[p] + 2];
                        //aSum += data[closePixelArr[p] + 3];
                    }
                }
                data[i] = rSum / 4;
                data[i+1] = gSum / 4;
                data[i+2] = bSum / 4;
                //data[i+3] = aSum / 4; // 8 = closePixelArr.length
            }
        }
        console.log(tmpCtx);
        tmpCtx.putImageData(imgData, 0, 0);
        tmpCtx.drawImage(tmpCanvas, 0, 0, iw, ih, 0, 0, cw, ((cw * ih) / iw));
        //ctx.drawImage(tmpCanvas, 0, 0, iw, ih, 0, 0, cw, ((cw * ih) / iw));
        console.log(cw);
        ctx.drawImage(tmpCanvas, 0, 0, cw, ((cw * ih) / iw), 0, 0, cw, ((cw * ih) / iw));
    }*/
}