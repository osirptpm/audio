/* 쓸지도 모르는 코드 */
    /*
    var cacheImageArr = []; // 이미지 캐싱
        var cacheImage = document.createElement('img');
        cacheImage.src = "/public/images/"+LargeOrSmall+"/technology-music-sound-things.jpg";
        cacheImageArr.push(cacheImage);
        console.log(cacheImageArr[0].src);
    */

$(function() {
    /* 초기 변수 선언 */
    var LargeOrSmall = (window.innerWidth > 767) ? 'L' : 'S',
        logo_offsetHeight = document.getElementById("logo").offsetHeight,
        signUpForm_offsetHeight,
        resizeTimer = null,
        jq_login_inBox = $("#login_inBox"),
        jq_metaThemeColor = $("meta[name=theme-color]"),
        tempValue,
        PIEFO = new PageInExecuteFuncOnce(); // 페이지 스크롤 관련 이벤트 함수
        //CP = new circleMenu(document.getElementById("circlePlayer")), // circle 메뉴 함수
        //ja_backgroundBlur_canvas = document.getElementById("backgroundBlur_canvas"), // 블러 캠버스
        //ja_canvasParent = document.getElementById("backgroundDiv_outbox"), // 블러 캠버스 크기 상속 요소
        //ja_albumCover = document.getElementsByClassName("circleMenu_albumCover")[0], // 블러 이미지 원본
        //blurImg = new CreateBlurImage(ja_backgroundBlur_canvas, ja_albumCover); // blur 이미지 생성 함수

    // circle 메뉴 생성
    //CP.setDegree = 225;
    //CP.createCircleMenu();

    /* 초기화 */
    var a = function () {
        document.getElementById("backgroundDiv_one").classList.add("display_block");
        document.getElementById("backgroundDiv_three").classList.add("display_block");
        document.getElementById("backgroundDiv_eight").classList.add("display_block");
        // 블러 캔버스 크기 지정
        //ja_backgroundBlur_canvas.width = ja_canvasParent.offsetWidth;
        //ja_backgroundBlur_canvas.height = ja_canvasParent.offsetHeight;
        // 스크롤 이동 상태에서 새고로침시 상태 유지를 위함
        loginPage_scroll();
    };

    /* 돔 로드 후 사이트 로딩 */
    $.ajax({
        type: "post",
        url: "/application/controllers/Controller.php",
        data : {'firstLoad': null},
        success: function(data) {
            console.log('가입창로딩');
            $("#signUp_outBox").html(data);
            signUpForm_offsetHeight = document.getElementById("signUp_form").offsetHeight;

            a();
            delete a; // 한번 실행후 리소스 반환
            console.log("돔레디");
        }
    });


    // 감상모드 로그인
    document.getElementById("mailAddr").onkeyup = function(event){
        var keycod = event.which || event.keyCode || event.charCode;
        if(keycod == 13){
            // mailAddr에서 아이디 가져오기
            var mailAddr = this.value;
            console.log(mailAddr);
            this.blur();
            document.getElementById("backgroundDiv_three").classList.add("translate_back");
            //document.getElementById("backgroundDiv_eight").classList.remove("display_block");
            //document.getElementById("circlePlayer").classList.add("translate_back");
            document.getElementById("pageDiv_inBox").classList.add("view_pageTwo");

        }
    }

    $("#question_btn").on("click", function () {
        $("body, html").animate({
            scrollTop: $("#signUp_outBox").offset().top
        }, 5000);
        console.log("클릭");
    });
    $(document).on("click", ".circleMenu_controlDiv", function () {
        //CP.toggleChildCircle();
    });

    //var ja_albumCover = $("#circleMenu_albumCover").get(0);
    //ja_albumCover.src = "http://osi.iptime.org/cloud/osi/WebPlayer/covers/01%20Say%20Something.jpeg";
    //ja_albumCover.src = "http://is4.mzstatic.com/image/thumb/Music5/v4/aa/e9/10/aae91080-46bc-7d91-5ef2-31fd858c172b/source/1000x1000.jpg";
    //ja_albumCover.src = "/public/images/L/technology-music-sound-things.jpg";
    //ja_albumCover.src = "http://osi.iptime.org/cloud/osi/WebPlayer/covers/01.%20Kiss%20My%20Lips.jpeg";
    //ja_albumCover.src = "http://www.brownsstationery.com.au/prod/pr_Products/thimage/pic1967.gif";


    //blurImg.drawBlurImage(100, 4, "cover");
    //ja_albumCover.src = "http://osi.iptime.org/cloud/osi/WebPlayer/covers/01.%20Kiss%20My%20Lips.jpeg";
    //blurImg.drawBlurImage(100, 4, "cover");

    // 메일주소 아이디부분을 이름으로 자동 입력
    $(document).on("keyup", "#signUp_Email", function() {
        if(document.getElementById("signUp_Email").value.indexOf("@", 0) == -1) {
            document.getElementById("signUp_Name").value = document.getElementById("signUp_Email").value;
        }
    });
    $(document).on("submit", "#signUp_form", function() {
        $theForm = $(this);
        if(document.getElementById("signUp_Passwd").value != document.getElementById("signUp_Passwd_check").value) {
            console.log('틀림');
            return false;
        }
         $.ajax({
            beforeSend: function() {
            },
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                    //	console.log(percentComplete);
                    //	$('.progress').css({
                    //		width: percentComplete * 100 + '%'
                    //	});
                    //	if (percentComplete === 1) {
                    //		$('.progress').addClass('hide');
                    //	}
                    }
                }, false);
                xhr.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                    //	console.log(percentComplete);
                    //	$('.progress').css({
                    //		width: percentComplete * 100 + '%'
                    //	});
                    }
                }, false);
                return xhr;
            },
            type: $theForm.attr("method"),
            url: $theForm.attr("action"),
            data: $theForm.serialize(),
            success: function(data) {
                console.log(data);
            }
        });
        // 기본 동작 취소
        return false;
    });

    window.addEventListener("scroll", loginPage_scroll);
    window.addEventListener("resize", function( ) {
        resizeTimer = DelayTimeout(resizeTimer, resizeDone, 300); // 딜레이 실행 함수
    }, false );

    /* 함수 모음 */
    // 스크롤 이벤트 함수
    function loginPage_scroll() {
        PIEFO.setAllData(document, window);
        PIEFO.pageInFunc("END", function () { // 페이지 끝일때
            jq_metaThemeColor.attr("content", "#ef5350");
        }, function () { // 페이지 끝에서 벗어날때
            jq_metaThemeColor.attr("content", "#ffffff"); // END의 코드가 먼저 와야함
        });
        PIEFO.pageInFunc(1, function () { // 1 페이지 일때
            jq_metaThemeColor.attr("content", "#5593C2");
            document.getElementById("backgroundDiv_two").classList.remove("display_block");
        }, function () { // 1 페이지 아닐때
            jq_metaThemeColor.attr("content", "#ffffff");
            document.getElementById("backgroundDiv_two").classList.add("display_block");
        });
        // 로고가 더 빨리 위로 올라가며 투명해짐
        jq_login_inBox.css({
            "transform": "matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, "+( -(tempValue = (1-PIEFO.currentScrollTop/logo_offsetHeight < 0) ? logo_offsetHeight : PIEFO.currentScrollTop) )+", 0, 1)",
            "opacity": 1-tempValue/logo_offsetHeight
        });
    }
    // 창 크기 변경되었을 때 실행 함수
    function resizeDone() {
        console.log("리사이즈");
        LargeOrSmall = (window.innerWidth > 767) ? 'L' : 'S'; // 브라우저 가로 길이 측정
        logo_offsetHeight = document.getElementById("logo").offsetHeight; //
        signUpForm_offsetHeight = document.getElementById("signUp_form").offsetHeight;
        //CP.setChildPosition(); // 원 메뉴 위치 재지정
        //ja_backgroundBlur_canvas.width = ja_canvasParent.offsetWidth;
        //ja_backgroundBlur_canvas.height = ja_canvasParent.offsetHeight;
        //blurImg.resizeNDrawCanvas(); //배경 블러이미지 리사이즈
    }
});
$(window).on("load", function () {
    console.log("로드끝");
    $(".opacity_toggle").removeClass("opacity_toggle");
    document.getElementById("mailAddr").focus();

});