$(function () {
    var ja_player = document.getElementById("player"),
        ja_player_topBar = document.getElementById("player_topBar"),
        ja_myAudio = document.getElementById("myAudio"),
        //ja_playList = document.getElementById("player_board").children[0].children[0],
        ja_playList = document.getElementsByClassName("playingList")[0].children[0],
        ja_currentSongProgress_current = document.getElementById("currentSongProgress_current"),
        ja_currentSongProgressBar_current = document.getElementById("currentSongProgressBar_current"),
        ja_currentSongBufferedBar_current = document.getElementById("currentSongBufferedBar_current"),
        ja_songCurrentTimeChild_current = document.getElementById("songCurrentTimeChild_current"),
        ja_songDurationTimeChild_current = document.getElementById("songDurationTimeChild_current"),
        ja_play = document.getElementsByClassName("play")[0];
        
    var LargeOrSmall = (window.innerWidth > 767) ? 'L' : 'S',
        resizeTimer = null;//,
        //preAssocCol = "data_associate_artist";
    // function.js 함수들
    var VCBHP = new ViewControlByHashParam(), // 해시값에 따른 사용자 함수 설정
        VCBHPLoad = VCBHP.loadChainFunc(), // 해시값에 따른 사용자 함수 설정
        hashSC = new HashStateControl(), // 정규식으로 hash 조작 함수 add, remove, toggle
        MPC = new MediaPlaylistControl(ja_myAudio, ja_playList); // 첫번째 노래 로드

    // 남 외부 라이브러리
    var colorThief = new ColorThief();
    
    // 이 파일 함수
    var CCBCT = new colorChangeByColorThief();

    var swipe_player_selectList = new SwipeElement(document.getElementById("player_selectList"), false), // 터치 스와이프 함수
        swipe_player_selectList = swipe_player_selectList.loadChainFunc(), // 스와이프 이벤트 함수 지정을 위함
        swipe_songCoverSec_current = new SwipeElement(document.getElementById("songCoverSec_current"), true, true),
        swipe_songCoverSec_current = swipe_songCoverSec_current.loadChainFunc(),
        swipe_songTextSec_current = new SwipeElement(document.getElementById("songTextSec_current")),
        swipe_songTextSec_current = swipe_songTextSec_current.loadChainFunc(),
        swipe_songCtrlSec_current = new SwipeElement(document.getElementById("songCtrlSec_current")),
        swipe_songCtrlSec_current = swipe_songCtrlSec_current.loadChainFunc(); 


    /* 초기화 */
    var a = function () {
        //document.getElementById("backgroundDiv_three").classList.add("display_block");
        //document.getElementById("backgroundDiv_three").classList.add("translate_back");
        //if(LargeOrSmall == "S") document.getElementById("player").classList.add("mobileMode");
        resizeDone();
        // 스크롤바 숨기기
        var ja_selectListDiv = document.getElementById("selectListDiv"),
            selectListDivWidth = ja_selectListDiv.offsetWidth + ja_selectListDiv.offsetWidth - ja_selectListDiv.clientWidth;
        ja_selectListDiv.style.width = selectListDivWidth+"px";

        // 해시값에 따른 사용자 함수 설정
        VCBHPLoad.setKeyNFunc({
            "view_playingSec": {
                "on": function () {
                    ja_player.classList.add("view_playingSec");
                    CCBCT.changeColor(ja_play);
                },
                "off": function () { 
                    ja_player.classList.remove("view_playingSec");
                    CCBCT.resetColor(ja_play);
                }
            },
            "view_associateSec": {
                "on": function () { ja_player.classList.add("view_associateSec"); },
                "off": function () { ja_player.classList.remove("view_associateSec"); }
            },
            "view_player_selectList": {
                "on": function () { ja_player.classList.add("view_player_selectList"); },
                "off": function () { ja_player.classList.remove("view_player_selectList"); }
            }/*,
            "data_associate_artist": {
                "on": function () { associateList_ajax(); }
            },
            "data_associate_album": {
                "on": function () { associateList_ajax(); }
            },
            "data_associate_genre": {
                "on": function () { associateList_ajax(); }
            }*/ //해시로 관련 목록 조작 코드
        });
        // 해시 초기값 추가
		//hashSC.add("data_associate_artist"); //해시로 관련 목록 조작 코드
        // 해시값에 따른 사용자 함수 실행
        VCBHP.checkAllFunc();
    };

    a();
    delete a; // 한번 실행후 리소스 반환

    // 화면 조작
    $("#songCtrlSec_current, #songTextSec_current").on("click", function (event) {
        event.stopPropagation();
        hashSC.remove("view_associateSec");
        hashSC.toggle("view_playingSec");
    });
    $(".songAssocCtrlBtn_current").on("click", function (event) {
        event.stopPropagation();
        hashSC.add("view_associateSec");
        $(".songAssocCtrlBtn_current").removeClass("selectAssocBtn");
        this.classList.add("selectAssocBtn");
        associateList_ajax();
        
        /*var curAssocCol = "data_associate_"+this.getAttribute("data-assocValue");
        hashSC.replace(preAssocCol,curAssocCol);
        preAssocCol = curAssocCol;*/ //해시로 관련 목록 조작 코드
    });

    // 플레이 리스트 관련 jquery 이벤트
    $(".loadList").on("click", function () {
        // 해당 리스트 불러오기
        loadPlaylist(this.getAttribute("data-loadList"));
    });


    // 재생 관련 jquery 이벤트
    $(".prev").on("click", function (event) {
        event.stopPropagation();
        MPC.prev();
    });
    $(".play").on("click", function (event) {
        event.stopPropagation();
		MPC.PP();
    });
    $(".next").on("click", function (event) {
        event.stopPropagation();
        MPC.next();
    });
    $(document).on("click", "#player_board .songSec", function () {
        MPC.thisTrack(this);
    });
    $(document).on("click", "#songAssocSec_current .songSec", function () {
        ja_playList.innerHTML = this.parentElement.innerHTML;
        var equalElement = $("#player_board [origin_id="+this.getAttribute("origin_id")+"]")[0];
        MPC.thisTrack(equalElement); // 클릭했던 요소와 같은 요소가 재생
    });
    
    // 터치 프로그레스 조작
    $("#currentSongTouchBar_current").on({
        "touchstart": function (event) {
            event.preventDefault();
            event.stopPropagation();
            MPC.getAudioElement.currentTime = MPC.getAudioElement.duration * event.touches[0].clientX / ja_currentSongProgress_current.offsetWidth;
        },
        "touchmove": function (event) {
            event.preventDefault();
            event.stopPropagation();
            MPC.getAudioElement.currentTime = MPC.getAudioElement.duration * event.touches[0].clientX / ja_currentSongProgress_current.offsetWidth;
        }
    });
    // 마우스 프로그레스 조작
    var isClicking = false,
        touchBarX = 0;
    $("#currentSongTouchBar_current").on("mousedown", function (event) {
        event.preventDefault();
        event.stopPropagation();
        touchBarX = event.clientX - event.offsetX;
        isClicking = true;
        MPC.getAudioElement.currentTime = MPC.getAudioElement.duration * event.offsetX / ja_currentSongProgress_current.offsetWidth;
    });
    $(document).on({
        "mousemove": function (event) {
            if (isClicking) {
                event.preventDefault();
                event.stopPropagation();
                MPC.getAudioElement.currentTime = MPC.getAudioElement.duration * (event.clientX - touchBarX) / ja_currentSongProgress_current.offsetWidth;
            }
        },
        "mouseup": function (event) {
            isClicking = false;
        }
    });

    ja_myAudio.addEventListener("play", PPClass); // css 조절 class 토글
    ja_myAudio.addEventListener("pause", PPClass); // css 조절 class 토글
    ja_myAudio.addEventListener("loadstart", newSongLoadStart); // 현재 곡 관련 목록 ajax 요청
    ja_myAudio.addEventListener("durationchange", newSongDurationChange); // 현재 곡 관련 목록 ajax 요청
    ja_myAudio.addEventListener("loadeddata", PP); // 이전에 재생/일시중지였는지 판단
    ja_myAudio.addEventListener("timeupdate", playbackProgress);
    ja_myAudio.addEventListener("progress", downloadProgress);
    ja_myAudio.addEventListener("ended", MPC.next); // 다음곡
    


    window.addEventListener("hashchange",  VCBHP.checkAllFunc);
    //window.addEventListener("scroll", playerPage_scroll);
    window.addEventListener("resize", function( ) {
        resizeTimer = DelayTimeout(resizeTimer, resizeDone, 300); // 딜레이 실행 함수
    }, false );

    /* 이벤트 바인드 */

    // 터치 스와이프 이벤트
    swipe_player_selectList.swipeLeft(function () {
        hashSC.remove("view_player_selectList");
    }).swipeRight(function () {
        hashSC.add("view_player_selectList");
    });
    swipe_songCoverSec_current.swipeLeft(function () {
        MPC.next();
    }).swipeRight(function () {
        MPC.prev();
    });
    swipe_songTextSec_current.swipeUp(function () {
        if(hashSC.hasHash("view_playingSec")) {
            hashSC.add("view_associateSec");
        } else {
            hashSC.add("view_playingSec");
        }
    }).swipeDown(function () {
        if(hashSC.hasHash("view_associateSec")) {
            hashSC.remove("view_associateSec");
        } else {
            hashSC.remove("view_playingSec");
        }
    });
    swipe_songCtrlSec_current.swipeUp(function () {
        if(hashSC.hasHash("view_playingSec")) {
            hashSC.add("view_associateSec");
        } else {
            hashSC.add("view_playingSec");
        }
    }).swipeDown(function () {
        if(hashSC.hasHash("view_associateSec")) {
            hashSC.remove("view_associateSec");
        } else {
            hashSC.remove("view_playingSec");
        }
    });

    /* 함수 모음 */
    // 창 크기 변경되었을 때 실행 함수
    function resizeDone() {
        console.log("리사이즈");
        LargeOrSmall = (window.innerWidth > 767) ? 'L' : 'S'; // 브라우저 가로 길이 측정
        if(LargeOrSmall == "S") {
            ja_playList.classList.remove("list_type_B");
            ja_playList.classList.add("list_type_A");
        } else {
            ja_playList.classList.remove("list_type_A");
            ja_playList.classList.add("list_type_B");
        }
    }
    var lastScroll = 0;
    function playerPage_scroll() {
        var st = $(this).scrollTop();
		if (st < lastScroll){
			//스크롤 올릴때
			//hashSC.add("view_player_topBar");
            ja_player.classList.add("view_player_topBar");
		} else {
			//스크롤 내릴때
			//hashSC.remove("view_player_topBar");
            ja_player.classList.remove("view_player_topBar");
		}
		//마지막 스크롤 위치 동기화
		lastScroll = st;
    }

    // 미디어 재생 관련 함수
    // 새 곡이 로드되기 시작하면
    function newSongLoadStart() {
        var title = MPC.getTrack.getElementsByClassName("songTitle_list")[0].textContent,
            artist = MPC.getTrack.getElementsByClassName("songArtist_list")[0].textContent,
            coverSrc = MPC.getTrack.getElementsByClassName("songCover_list")[0].src;
        document.title = title+" - "+artist;
        document.getElementById("songCover_current").style.backgroundImage = "url(\""+coverSrc+"\")";
        document.getElementById("songTitle_current").textContent = title;
        document.getElementById("songArtist_current").textContent = artist;
        document.getElementById("songAlbum_current").textContent = MPC.getTrack.getElementsByClassName("songAlbum_list")[0].textContent;
        document.getElementById("songSimpleCover_current").src = coverSrc;
        document.getElementById("songSimpleTitle_current").textContent = title;
        document.getElementById("songSimpleArtist_current").textContent = artist;
        ja_songCurrentTimeChild_current.textContent = "00:00";
        ja_currentSongProgressBar_current.style.transform = "translateX(-100%)";
        ja_currentSongBufferedBar_current.style.transform = "translateX(-100%)";
        // 현재 곡 관련 목록 ajax 요청
        associateList_ajax();
        // 곡 앨범아트에 따라 색바꾸기
        CCBCT.setColor();
        if(LargeOrSmall != "S" || ja_player.classList.contains("view_playingSec")) {
            CCBCT.changeColor(ja_play);
        }
        CCBCT.changeColor(false);
    }
    function newSongDurationChange() {
        var duration = Math.round(MPC.getAudioElement.duration);
        ja_songDurationTimeChild_current.textContent = 
            (((""+Math.floor(duration / 60)).length == 1) ? "0"+Math.floor(duration / 60) : Math.floor(duration / 60))
             +":"+ 
            (((""+duration % 60).length == 1) ? "0"+duration % 60 : duration % 60);
    }
    // 현재 곡 관련 목록 ajax 요청
    function associateList_ajax() {
        /*var locationhash = location.hash,
            patt = new RegExp("#data_associate_[0-9a-zA-Z]*$|#data_associate_[0-9a-zA-Z]*#", "g");
        var assocColumn = patt.exec(locationhash)[0].replace(/#/g, "").replace("data_associate_", ""),*/ //해시로 관련 목록 조작 코드
        var assocColumn = document.getElementsByClassName("selectAssocBtn")[0].getAttribute("data-assocValue"),
            assocValue;
        if (assocColumn == "artist") {
            assocValue = MPC.getTrack.getElementsByClassName("songArtist_list")[0].textContent;
        } else if (assocColumn == "album") {
            assocValue = MPC.getTrack.getElementsByClassName("songAlbum_list")[0].textContent;
        } else if (assocColumn == "genre") {
            assocValue = MPC.getTrack.getElementsByClassName("songGenre_list")[0].textContent;
        }
        if (assocValue.length == 0) {
            document.getElementById("songAssocSec_current").children[0].innerHTML = "관련 목록을 가져올 정보가 없습니다.";
        } else {
            $.ajax ({
                beforeSend: function() {
                },
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                        //	console.log(percentComplete);
                        //	$('.progress').css({
                        //		width: percentComplete * 100 + '%'
                        //	});
                        //	if (percentComplete === 1) {
                        //		$('.progress').addClass('hide');
                        //	}
                        }
                    }, false);
                    xhr.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                        //	console.log(percentComplete);
                        //	$('.progress').css({
                        //		width: percentComplete * 100 + '%'
                        //	});
                        }
                    }, false);
                    return xhr;
                },
                type: "GET", // POST 로 전송
                url: "/application/controllers/Controller.php", // 호출 URL
                data: {
                    "assocColumn": assocColumn,
                    "assocValue": assocValue
                }, // 파라메터 정보 전달
                success:function(data){
                    document.getElementById("songAssocSec_current").children[0].innerHTML = data;
                    //var ja_songAssocSec_current = document.getElementById("songAssocSec_current").children[0];
                    //HideScrollBar(ja_songAssocSec_current);
                }
            });
        }
    }
    // 재생인지 판단해 css 조절 class 토글
    function PPClass() {
		if (ja_myAudio.paused == true && ja_myAudio.ended != true) {
            $(".play")[0].classList.remove("pause");
		} else {
            $(".play")[0].classList.add("pause");
		}
    }
    // 재생 중이 었는지 판단
    function PP() {
		if ($(".play").hasClass("pause")) {
            ja_myAudio.play();
        }
    }
    function playbackProgress() {
        ja_currentSongProgressBar_current.style.transform = "translateX(calc(-100% + "+MPC.getProgress+"%))";
        var currentTime = Math.round(MPC.getAudioElement.currentTime);
        ja_songCurrentTimeChild_current.textContent = 
            (((""+Math.floor(currentTime / 60)).length == 1) ? "0"+Math.floor(currentTime / 60) : Math.floor(currentTime / 60))
             +":"+ 
            (((""+currentTime % 60).length == 1) ? "0"+currentTime % 60 : currentTime % 60);
    }
    function downloadProgress() {
        ja_currentSongBufferedBar_current.style.transform = "translateX(calc(-100% + "+MPC.getBuffered+"%))";
    }
    // 지정되있는 요소들 앨범아트에 따라 색 바꾸기
    function colorChangeByColorThief() {
        var dominantColor,
            rgbaStr,
            timer;
        
        this.setColor = function () {
            _setColor();
        }
        this.changeColor = function (el) { // true 면 전부 false 면 일부 제외, 아니면 해당 el만
            _changeColor(el);
        }
        this.resetColor = function (el) { // true 면 전부 false 면 일부 제외, 아니면 해당 el만
            _resetColor(el);
        }
        function _setColor() {
            dominantColor = colorThief.getColor(MPC.getTrack.getElementsByClassName("songCover_list")[0]);
            rgbaStr = "rgba("+dominantColor[0]+", "+dominantColor[1]+", "+dominantColor[2]+", 1)";
        }
        function _changeColor(el) {
            if(el == true || el == false) {
                timer = DelayTimeout(timer, function(){
                    document.getElementsByName("theme-color")[0].setAttribute("content", rgbaStr);
                }, 1000);
                ja_player_topBar.style.backgroundColor = rgbaStr;
            }
            if(el == true || ja_play.isSameNode(el)) { // ja_play 만
                ja_play.style.backgroundColor = rgbaStr;
            }
        }
        function _resetColor(el) {
            if(el == true || el == false) { // 전부 리셋
                ja_play.style.backgroundColor = "rgba(255, 255, 255, 1)";
            }
            if(el == true || ja_play.isSameNode(el)) { // ja_play 만
                ja_play.style.backgroundColor = "rgba(255, 255, 255, 1)";
            }
        }

        //console.log(colorThief.getColor(MPC.getTrack.getElementsByClassName("songCover_list")[0]));
        //console.log(colorThief.getPalette(MPC.getTrack.getElementsByClassName("songCover_list")[0]), 8);
    }
    // 플레이 리스트 로드 ajax
    function loadPlaylist(loadList) {
        $.ajax ({
            beforeSend: function() {
            },
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                    //	console.log(percentComplete);
                    //	$('.progress').css({
                    //		width: percentComplete * 100 + '%'
                    //	});
                    //	if (percentComplete === 1) {
                    //		$('.progress').addClass('hide');
                    //	}
                    }
                }, false);
                xhr.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                    //	console.log(percentComplete);
                    //	$('.progress').css({
                    //		width: percentComplete * 100 + '%'
                    //	});
                    }
                }, false);
                return xhr;
            },
            type: "GET", // POST 로 전송
            url: "/application/controllers/Controller.php", // 호출 URL
            data: {
                "favorite": loadList
            }, // 파라메터 정보 전달
            success:function(data){
                document.getElementById("player_board").children[0].children[0].innerHTML = data;
            }
        });
    }
});