pushd %~dp0
set formats=*.mp3 *.wav *.m4a *.wma *.flac
set presets=-f mp3 -id3v2_version 3 -write_id3v1 1 -b:a 192k -c:v copy
set TEMP_FILE=temp

if not exist ..\original_file md ..\original_file
if not exist ..\musics md ..\musics

for %%f in (%formats%) do (
    if not "%%~xf%" == ".mp3" (
	F:\Web\ffmpag\ffmpeg.exe -i "%%f" %presets% %TEMP_FILE% 2>..\output.txt
        move /Y "%%f" ..\original_file
        move /Y %TEMP_FILE% "..\musics\%%~nf%.mp3"
		del "..\output.txt"
    ) ELSE (
        copy /Y "%%f" ..\original_file
        move /Y "%%f" "..\musics\%%f%"
    )
)